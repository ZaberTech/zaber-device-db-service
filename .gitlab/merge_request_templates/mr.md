Checklist

* Have you bumped the version? (Any change in API requires to bump the version in `pkg/api/service.go`)
* Have you update the documentation (https://gitlab.izaber.com/software-internal/zaber-device-db-service-docs)?
