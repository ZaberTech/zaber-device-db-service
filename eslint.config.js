const tsEslint = require('typescript-eslint');
const { config: zaberEslintConfig } = require('@zaber/eslint-config');
const path = require('node:path');
const { includeIgnoreFile } = require('@eslint/compat');

module.exports = tsEslint.config(
  includeIgnoreFile(path.resolve(__dirname, '.gitignore')),
  {
    files: ['**/*.js'],
    extends: [
      zaberEslintConfig.javascript,
    ],
  },
  {
    files: ['**/*.test.js', 'src/test/**/*.js'],
    languageOptions: {
      globals: {
        describe: 'readonly',
        it: 'readonly',
      }
    }
  }
);
