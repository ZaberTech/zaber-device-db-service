# zaber-device-db-service

## Testing

Set `UPDATE_SNAPSHOTS=1` if you want to update the snapshots.

## Device DB Schema

The code can only work with one (and only one) schema version of Device DB file.
The version is specified in `pkg\device_db\queries.go` as `dbAPIVersion` variable.
The value is checked against `SwWeb_Version` during initialization.
Any changes to the schema must be accompanied by change of the version.
The queries must only use views with prefix `SwWeb_` to ensure compatibility.

The code should accomodate any changes in backwards compatible maner most likely resulting in minor API version bump.
That way, anything using the web service (as a compatibility layer) remains backwards compatible.
Anything else that uses the code directly (such as ZML in file mode) will need to download a new DB file
otherwise the aformentioned check results in an error.
