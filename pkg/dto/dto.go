package dto

type Device struct {
	DeviceID  int              `json:"device_id"`
	ProductID int              `json:"product_id"`
	Fw        *FirmwareVersion `json:"fw,omitempty"`
	Name      string           `json:"name"`
}

type DeviceInfo struct {
	Device           *Device          `json:"device"`
	Capabilities     []string         `json:"capabilities"`
	SaveStateVersion int              `json:"save_state_version,omitempty"`
	ConversionTable  *ConversionTable `json:"conversion_table"`
	CommandTree      *CommandNode     `json:"command_tree"`
	Settings         *SettingsTable   `json:"settings"`
	Peripherals      []*Peripheral    `json:"peripherals,omitempty"`
}

type Peripheral struct {
	Device          *Device          `json:"device"`
	Capabilities    []string         `json:"capabilities"`
	ConversionTable *ConversionTable `json:"conversion_table"`
	CommandTree     *CommandNode     `json:"command_tree"`
	Settings        *SettingsTable   `json:"settings"`
}

type ConversionTableRow struct {
	ContextualDimensionID   int      `json:"contextual_dimension_id"`
	ContextualDimensionName string   `json:"contextual_dimension_name"`
	DimensionName           string   `json:"dimension_name"`
	FunctionName            string   `json:"function_name"`
	Scale                   *float64 `json:"scale"`
}
type ConversionTable struct {
	Rows []*ConversionTableRow `json:"rows"`
}

type CommandNode struct {
	Command               string         `json:"command"`
	IsParam               bool           `json:"is_param,omitempty"`
	IsCommand             bool           `json:"is_command,omitempty"`
	ContextualDimensionID int            `json:"contextual_dimension_id,omitempty"`
	ParamType             string         `json:"param_type,omitempty"`
	ParamArity            int            `json:"param_arity,omitempty"`
	Values                []string       `json:"values,omitempty"`
	Nodes                 []*CommandNode `json:"nodes,omitempty"`
}

type SettingsTable struct {
	Rows []*SettingsTableRow `json:"rows"`
}
type SettingsTableRow struct {
	Name                  string  `json:"name"`
	ContextualDimensionID int     `json:"contextual_dimension_id,omitempty"`
	ParamType             string  `json:"param_type"`
	DecimalPlaces         int     `json:"decimal_places"`
	ReadAccessLevel       int     `json:"read_access_level,omitempty"`
	WriteAccessLevel      int     `json:"write_access_level,omitempty"`
	Visibility            string  `json:"visibility"`
	DefaultValue          *string `json:"default_value,omitempty"`
	Realtime              bool    `json:"realtime,omitempty"`
}

type BinarySettingsTable struct {
	Rows []*BinarySettingsTableRow `json:"rows"`
}
type BinarySettingsTableRow struct {
	Name                  string `json:"name"`
	ContextualDimensionID int    `json:"contextual_dimension_id,omitempty"`
	SetCommand            int    `json:"set_command,omitempty"`
	GetCommand            int    `json:"get_command,omitempty"`
}

type BinaryCommandsTable struct {
	Rows []*BinaryCommandsTableRow `json:"rows"`
}
type BinaryCommandsTableRow struct {
	Name                        string `json:"name"`
	Command                     int    `json:"command"`
	HasParam                    bool   `json:"has_param,omitempty"`
	ParamContextualDimensionID  int    `json:"param_contextual_dimension_id,omitempty"`
	ReturnContextualDimensionID int    `json:"return_contextual_dimension_id,omitempty"`
}

type BinaryRepliesTable struct {
	Rows []*BinaryRepliesTableRow `json:"rows"`
}
type BinaryRepliesTableRow struct {
	Name                  string `json:"name"`
	Reply                 int    `json:"reply"`
	ContextualDimensionID int    `json:"contextual_dimension_id,omitempty"`
}

type BinaryDeviceInfo struct {
	Device          *Device              `json:"device"`
	Capabilities    []string             `json:"capabilities"`
	ConversionTable *ConversionTable     `json:"conversion_table"`
	Commands        *BinaryCommandsTable `json:"commands,omitempty"`
	Replies         *BinaryRepliesTable  `json:"replies,omitempty"`
	Settings        *BinarySettingsTable `json:"settings"`
	Peripheral      *BinaryPeripheral    `json:"peripheral,omitempty"`
}

type BinaryPeripheral struct {
	Device          *Device              `json:"device"`
	Capabilities    []string             `json:"capabilities"`
	ConversionTable *ConversionTable     `json:"conversion_table"`
	Commands        *BinaryCommandsTable `json:"commands,omitempty"`
	Replies         *BinaryRepliesTable  `json:"replies,omitempty"`
	Settings        *BinarySettingsTable `json:"settings"`
}

type SupportedPeripheral struct {
	PeripheralID int    `json:"peripheral_id"`
	ProductID    int    `json:"product_id"`
	Name         string `json:"name"`
}
type SupportedPeripherals struct {
	Peripherals []*SupportedPeripheral `json:"peripherals"`
}

type ServoTuningSimpleParam struct {
	DataType      string               `json:"DataType"`
	MaxValueLabel string               `json:"MaxValueLabel"`
	MinValueLabel string               `json:"MinValueLabel"`
	ParameterName string               `json:"ParameterName"`
	Values        map[string][]float64 `json:"Values"`
	DefaultValue  *float64             `json:"DefaultValue,omitempty"`
}

type ServoTuningSimpleGuidance struct {
	Algorithm      string                   `json:"Algorithm"`
	UserParameters []ServoTuningSimpleParam `json:"UserParameters"`
	Version        string                   `json:"Version"`
}

type ServoTuningSimpleGuidanceEncoded struct {
	GuidanceData []ServoTuningSimpleGuidance `json:"GuidanceData"`
}

type ServoTuningInfo struct {
	Algorithm        string                      `json:"Algorithm"`
	AlgorithmVersion int                         `json:"AlgorithmVersion"`
	Forque           float64                     `json:"Forque"`
	CarriageInertia  float64                     `json:"CarriageInertia"`
	MotorInertia     float64                     `json:"MotorInertia"`
	DriveRatio       float64                     `json:"DriveRatio"`
	Guidance         []ServoTuningSimpleGuidance `json:"Guidance"`
}

type PeripheralDefaultSettings struct {
	PeripheralID int               `json:"peripheral_id"`
	Defaults     map[string]string `json:"defaults"`
}

type DefaultSettings struct {
	DeviceID    int                          `json:"device_id"`
	Fw          *FirmwareVersion             `json:"fw"`
	Defaults    map[string]string            `json:"defaults"`
	Peripherals []*PeripheralDefaultSettings `json:"peripherals,omitempty"`
}

type FoundProduct struct {
	DeviceID     int    `json:"deviceId,omitempty"`
	PeripheralID int    `json:"peripheralId,omitempty"`
	Name         string `json:"name"`
}

type FoundProducts struct {
	Products []*FoundProduct `json:"products"`
}
