package dto

import "testing"

func TestFirmwareVersion_IsHigherOrSame(t *testing.T) {
	fw := FirmwareVersion{1, 2, 3}
	if fw.IsHigherOrSame(FirmwareVersion{1, 2, 3}) != true {
		t.Error("1.2.3 >= 1.2.3")
	}
	if fw.IsHigherOrSame(FirmwareVersion{1, 1, 4}) != true {
		t.Error("1.2.3 >= 1.1.4")
	}
	if fw.IsHigherOrSame(FirmwareVersion{0, 3, 4}) != true {
		t.Error("1.2.3 >= 0.3.4")
	}
	if fw.IsHigherOrSame(FirmwareVersion{1, 2, 4}) != false {
		t.Error("1.2.3 < 1.2.4")
	}
	if fw.IsHigherOrSame(FirmwareVersion{1, 3, 3}) != false {
		t.Error("1.2.3 < 1.3.3")
	}
	if fw.IsHigherOrSame(FirmwareVersion{2, 2, 3}) != false {
		t.Error("1.2.3 < 2.2.3")
	}
}

func TestFirmwareVersion_Parse(t *testing.T) {
	fw, err := ParseFWVersion("1.2.3")
	if err != nil || fw != (FirmwareVersion{1, 2, 3}) {
		t.Error("ParseFWVersion(1.2.3)", err)
	}

	fw, err = ParseFWVersion("5")
	if err != nil || fw != (FirmwareVersion{5, -1, -1}) {
		t.Error("ParseFWVersion(5)", err)
	}

	fw, err = ParseFWVersion("1.2")
	if err != nil || fw != (FirmwareVersion{1, 2, -1}) {
		t.Error("ParseFWVersion(1.2)", err)
	}
}
