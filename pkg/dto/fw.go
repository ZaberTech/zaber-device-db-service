package dto

import (
	"fmt"
	"strconv"
	"strings"
)

type FirmwareVersion struct {
	Major int `json:"major"`
	Minor int `json:"minor"`
	Build int `json:"build"`
}

func ParseFWVersion(fwString string) (FirmwareVersion, error) {
	fw := FirmwareVersion{-1, -1, -1}
	parts := strings.Split(fwString, ".")
	if len(parts) >= 1 {
		if Major, err := strconv.Atoi(parts[0]); err != nil {
			return fw, err
		} else { //revive:disable-line:indent-error-flow
			fw.Major = Major
		}
	}
	if len(parts) >= 2 {
		if Minor, err := strconv.Atoi(parts[1]); err != nil {
			return fw, err
		} else { //revive:disable-line:indent-error-flow
			fw.Minor = Minor
		}
	}
	if len(parts) >= 3 {
		if build, err := strconv.Atoi(parts[2]); err != nil {
			return fw, err
		} else { //revive:disable-line:indent-error-flow
			fw.Build = build
		}
	}
	return fw, nil
}

func (fw FirmwareVersion) IsHigherOrSame(fwOther FirmwareVersion) bool {
	return fw.Major > fwOther.Major ||
		(fw.Major == fwOther.Major && fw.Minor > fwOther.Minor) ||
		(fw.Major == fwOther.Major && fw.Minor == fwOther.Minor && fw.Build >= fwOther.Build)
}

func (fw FirmwareVersion) String() string {
	var str string
	if fw.Major >= 0 {
		str += fmt.Sprintf("%d", fw.Major)
		if fw.Minor >= 0 {
			str += fmt.Sprintf(".%d", fw.Minor)
			if fw.Build >= 0 {
				str += fmt.Sprintf(".%d", fw.Build)
			}
		}
	}
	return str
}
