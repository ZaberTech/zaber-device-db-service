package client

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/api"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/errors"
)

const APIVersionHeader = "Api-Version"
const APIVersionQueryParam = "api-version"
const userAgentHeader = "User-Agent"

type Client interface {
	api.BaseServiceBasic
}

type client struct {
	baseURL   string
	userAgent string
}

type errorJSON struct {
	Message string `json:"message"`
}

func NewClient(baseURL string, userAgent string) (Client, errors.Error) {
	instance := &client{
		baseURL:   baseURL,
		userAgent: userAgent,
	}

	if _, err := url.Parse(instance.baseURL); err != nil {
		return nil, errors.New("Cannot parse the baseURL: " + err.Error())
	}

	return instance, nil
}

func checkHTTPError(response *http.Response) errors.Error {
	if 200 <= response.StatusCode && response.StatusCode <= 299 {
		return nil
	}

	body, _ := io.ReadAll(response.Body)

	var errorMsg string
	var errorJSON errorJSON

	if json.Unmarshal(body, &errorJSON) == nil {
		errorMsg = errorJSON.Message
	} else if body != nil {
		errorMsg = (string)(body)
	} else {
		errorMsg = "Unknown Error"
	}

	if response.StatusCode == 404 {
		return errors.NewType(errors.ErrorNoRecordFound, errorMsg)
	}

	return errors.Wrap(fmt.Errorf("Service failed %d: %s - %s", response.StatusCode, response.Status, errorMsg))
}

func (instance *client) doRequest(method string, requestURL string) (*http.Response, errors.Error) {
	log.Print(method, " ", requestURL)

	request, err := http.NewRequest(method, requestURL, nil)
	if err != nil {
		return nil, errors.Wrap(err)
	}
	request.Header.Add(APIVersionHeader, api.APIVersion)
	request.Header.Add(userAgentHeader, instance.userAgent)

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, errors.Wrap(err)
	}

	if httpErr := checkHTTPError(response); httpErr != nil {
		return nil, httpErr
	}

	return response, nil
}

func (instance *client) GetProductInformation(productQuery *api.GetProductInformationQuery) (*dto.DeviceInfo, errors.Error) {
	reqURL, _ := url.Parse(instance.baseURL)

	basePath := strings.TrimRight(reqURL.Path, "/")
	reqURL.Path = fmt.Sprintf("%s/device/%s", basePath, productQuery.DeviceID)

	query := reqURL.Query()
	if productQuery.FW != nil {
		query.Add("fw", productQuery.FW.String())
	}
	if productQuery.Modified {
		query.Add("modified", strconv.FormatBool(true))
	}

	somePeripheralModified := false
	for _, peripheral := range productQuery.Peripherals {
		query.Add("peripheral", peripheral.PeripheralID)
		if peripheral.Modified {
			somePeripheralModified = true
		}
	}

	if somePeripheralModified {
		for _, peripheral := range productQuery.Peripherals {
			query.Add("peripheral-modified", strconv.FormatBool(peripheral.Modified))
		}
	}

	reqURL.RawQuery = query.Encode()

	response, reqErr := instance.doRequest("GET", reqURL.String())
	if reqErr != nil {
		return nil, reqErr
	}

	var info dto.DeviceInfo
	err := json.NewDecoder(response.Body).Decode(&info)
	if err != nil {
		return nil, errors.Wrap(err)
	}

	// read rest of the body
	if _, err = io.ReadAll(response.Body); err != nil {
		return nil, errors.Wrap(err)
	}

	return &info, nil
}

func (instance *client) GetBinaryProductInformation(deviceID string, fw *dto.FirmwareVersion, peripheralID string) (*dto.BinaryDeviceInfo, errors.Error) {
	reqURL, _ := url.Parse(instance.baseURL)

	basePath := strings.TrimRight(reqURL.Path, "/")
	reqURL.Path = fmt.Sprintf("%s/device-binary/%s", basePath, deviceID)

	query := reqURL.Query()
	if fw != nil {
		query.Add("fw", fw.String())
	}
	if len(peripheralID) > 0 {
		query.Add("peripheral", peripheralID)
	}
	reqURL.RawQuery = query.Encode()

	response, reqErr := instance.doRequest("GET", reqURL.String())
	if reqErr != nil {
		return nil, reqErr
	}

	var info dto.BinaryDeviceInfo
	err := json.NewDecoder(response.Body).Decode(&info)
	if err != nil {
		return nil, errors.Wrap(err)
	}

	// read rest of the body
	if _, err = io.ReadAll(response.Body); err != nil {
		return nil, errors.Wrap(err)
	}

	return &info, nil
}

func (instance *client) GetServoTuningInfo(productID int) (*dto.ServoTuningInfo, errors.Error) {
	reqURL, _ := url.Parse(instance.baseURL)

	basePath := strings.TrimRight(reqURL.Path, "/")
	reqURL.Path = fmt.Sprintf("%s/product/%d/servo-tuning-info", basePath, productID)

	query := reqURL.Query()

	reqURL.RawQuery = query.Encode()

	response, reqErr := instance.doRequest("GET", reqURL.String())
	if reqErr != nil {
		return nil, reqErr
	}

	var version dto.ServoTuningInfo
	err := json.NewDecoder(response.Body).Decode(&version)
	if err != nil {
		return nil, errors.Wrap(err)
	}

	// read rest of the body
	if _, err = io.ReadAll(response.Body); err != nil {
		return nil, errors.Wrap(err)
	}

	return &version, nil
}
