package errors

type ErrorType int

const (
	ErrorGeneric                 ErrorType = 0
	ErrorNoRecordFound           ErrorType = 1
	ErrorNotImplemented          ErrorType = 2
	ErrorIncompatibleDatabase    ErrorType = 3
	ErrorChecksumNotAvailableYet ErrorType = 4
)

type Error interface {
	ErrorType() ErrorType
	Error() string
}

type errorData struct {
	errorType ErrorType
	message   string
}

func New(message string) Error {
	return &errorData{
		message:   message,
		errorType: ErrorGeneric,
	}
}
func NewType(errorType ErrorType, message string) Error {
	return &errorData{
		message:   message,
		errorType: errorType,
	}
}

func Wrap(err error) Error {
	if err == nil {
		return nil
	}
	return &errorData{
		message:   err.Error(),
		errorType: ErrorGeneric,
	}
}

func (instance *errorData) Error() string {
	return instance.message
}

func (instance *errorData) ErrorType() ErrorType {
	return instance.errorType
}
