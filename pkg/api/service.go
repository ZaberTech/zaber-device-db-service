package api

import (
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/errors"
)

const APIVersion = "1.15"

type GetProductInformationQuery struct {
	DeviceID    string
	FW          *dto.FirmwareVersion
	Modified    bool
	Peripherals []Peripheral
}

type Peripheral struct {
	PeripheralID string
	Modified     bool
}

type ProductSearchQuery struct {
	Name     string
	DeviceID string
	FW       *dto.FirmwareVersion
	Limit    int
}

type BaseServiceBasic interface {
	GetProductInformation(query *GetProductInformationQuery) (*dto.DeviceInfo, errors.Error)
	GetBinaryProductInformation(deviceID string, fw *dto.FirmwareVersion, peripheralID string) (*dto.BinaryDeviceInfo, errors.Error)
	GetServoTuningInfo(productID int) (*dto.ServoTuningInfo, errors.Error)
}

type BaseService interface {
	BaseServiceBasic
	GetSupportedPeripherals(productID int) (*dto.SupportedPeripherals, errors.Error)
	GetDefaultSettings(deviceID string, fw *dto.FirmwareVersion, peripheralIDs []string) (*dto.DefaultSettings, errors.Error)
	SearchProduct(query *ProductSearchQuery) (*dto.FoundProducts, errors.Error)
}
