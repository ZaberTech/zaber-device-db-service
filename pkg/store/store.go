package store

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/api"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/errors"
)

const cacheVersion = 1

type serviceStore struct {
	service api.BaseServiceBasic
	store   *fsStore
}

type ServiceStore interface {
	api.BaseServiceBasic
	ExchangeUnderlayingService(underlayingService api.BaseServiceBasic)
}

func NewServiceStore(underlayingService api.BaseServiceBasic, storeDirectory string) ServiceStore {
	return &serviceStore{
		service: underlayingService,
		store:   newFSStore(storeDirectory),
	}
}

func getFileName(query *api.GetProductInformationQuery) string {
	file := fmt.Sprintf("cache%d_api%s_dev%s", cacheVersion, api.APIVersion, query.DeviceID)
	if query.FW != nil {
		file += "_fw" + query.FW.String()
	}
	if query.Modified {
		file += "_mod"
	}
	if len(query.Peripherals) > 0 {
		peripherals := make([]string, len(query.Peripherals))
		for i, peripheral := range query.Peripherals {
			peripherals[i] = peripheral.PeripheralID
			if peripheral.Modified {
				peripherals[i] += "mod"
			}
		}
		file += "_ps" + strings.Join(peripherals, "-")
	}

	return file + ".json"
}

func getFileNameBinary(deviceID string, fw *dto.FirmwareVersion, peripheralID string) string {
	file := fmt.Sprintf("cache%d_api%s_dev%s", cacheVersion, api.APIVersion, deviceID)
	if fw != nil {
		file += "_fw" + fw.String()
	}
	if len(peripheralID) > 0 {
		file += "_ps" + peripheralID
	}

	return file + "_binary.json"
}

func (instance *serviceStore) tryToRetrieve(file string, instantiate func() interface{}) (interface{}, bool, errors.Error) {
	storedData, isStored, err := instance.store.Read(file)
	if err != nil {
		log.Print("Cannot read the store: ", err)
		return nil, false, errors.New("Cannot read the store: " + err.Error())
	}

	if !isStored {
		return nil, false, nil
	}

	var data = instantiate()
	if err := json.Unmarshal(storedData, data); err != nil {
		log.Print("Store data malformed: ", err)
		return nil, false, nil
	}

	return data, true, nil
}

func (instance *serviceStore) storeData(file string, toStore interface{}) errors.Error {
	dataToStore, err := json.Marshal(toStore)
	if err != nil {
		log.Print("Cannot serialize data: ", err)
		return errors.Wrap(err)
	}

	if err := instance.store.Write(file, dataToStore); err != nil {
		log.Print("Cannot write to the store: ", err)
		return errors.New("Cannot write to the store: " + err.Error())
	}

	return nil
}

func newDeviceInfo() interface{} { return &dto.DeviceInfo{} }

func (instance *serviceStore) GetProductInformation(query *api.GetProductInformationQuery) (*dto.DeviceInfo, errors.Error) {
	file := getFileName(query)

	if info, ok, err := instance.tryToRetrieve(file, newDeviceInfo); ok || err != nil {
		if err != nil {
			return nil, err
		}
		return info.(*dto.DeviceInfo), nil
	}

	info, err := instance.service.GetProductInformation(query)
	if err != nil {
		return nil, err
	}

	if err := instance.storeData(file, info); err != nil {
		return nil, err
	}

	return info, nil
}

func newBinaryDeviceInfo() interface{} { return &dto.BinaryDeviceInfo{} }

func (instance *serviceStore) GetBinaryProductInformation(deviceID string, fw *dto.FirmwareVersion, peripheralID string) (*dto.BinaryDeviceInfo, errors.Error) {
	file := getFileNameBinary(deviceID, fw, peripheralID)

	if info, ok, err := instance.tryToRetrieve(file, newBinaryDeviceInfo); ok || err != nil {
		if err != nil {
			return nil, err
		}
		return info.(*dto.BinaryDeviceInfo), nil
	}

	info, err := instance.service.GetBinaryProductInformation(deviceID, fw, peripheralID)
	if err != nil {
		return nil, err
	}

	if err := instance.storeData(file, info); err != nil {
		return nil, err
	}

	return info, nil
}

func (instance *serviceStore) ExchangeUnderlayingService(underlayingService api.BaseServiceBasic) {
	instance.service = underlayingService
}

func newServoTuningInfo() interface{} { return &dto.ServoTuningInfo{} }

func (instance *serviceStore) GetServoTuningInfo(productID int) (*dto.ServoTuningInfo, errors.Error) {
	fileName := fmt.Sprintf("cache%d_api%s_servo_tuning_info_%d", cacheVersion, api.APIVersion, productID)

	info, ok, err := instance.tryToRetrieve(fileName, newServoTuningInfo)
	if err != nil {
		return nil, err
	}
	if ok {
		return info.(*dto.ServoTuningInfo), nil
	}

	tuningInfo, err := instance.service.GetServoTuningInfo(productID)
	if err != nil {
		return nil, err
	}

	if err := instance.storeData(fileName, tuningInfo); err != nil {
		return nil, err
	}

	return tuningInfo, nil
}
