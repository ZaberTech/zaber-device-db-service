package store

import (
	"os"
	"path/filepath"
)

type fsStore struct {
	directory string
}

func (c *fsStore) Read(file string) (resp []byte, ok bool, err error) {
	path := c.fileToPath(file)

	data, err := os.ReadFile(path)
	if err != nil {
		if os.IsNotExist(err) {
			// file does not exists
			return nil, false, nil
		}

		return nil, false, err
	}

	return data, true, nil
}

// TODO: use google/renameio once modified for windows

func (c *fsStore) writeToTmp(resp []byte) (string, error) {
	file, err := os.CreateTemp(c.directory, "tmp_")
	if err != nil {
		return "", err
	}

	tmpName := file.Name()

	if _, err = file.Write(resp); err != nil {
		_ = file.Close()
		_ = os.Remove(tmpName)
		return "", err
	}

	if err := file.Sync(); err != nil {
		_ = file.Close()
		_ = os.Remove(tmpName)
		return "", err
	}

	if err := file.Close(); err != nil {
		_ = os.Remove(tmpName)
		return "", err
	}

	return tmpName, nil
}

func (c *fsStore) Write(file string, resp []byte) error {
	if err := os.MkdirAll(c.directory, os.ModePerm); err != nil {
		return err
	}

	tmpName, err := c.writeToTmp(resp)
	if err != nil {
		return err
	}

	path := c.fileToPath(file)

	err = os.Rename(tmpName, path)
	if err != nil {
		_ = os.Remove(tmpName)
		return err
	}

	return nil
}

func (c *fsStore) fileToPath(file string) string {
	return filepath.Join(c.directory, file)
}

func newFSStore(directory string) *fsStore {
	return &fsStore{
		directory: directory,
	}
}
