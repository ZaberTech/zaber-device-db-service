package store

import (
	"os"
	"path/filepath"
	"testing"
	"time"

	"gotest.tools/v3/assert"
)

const testKey = "test"

var testDir = filepath.Join("..", "..", "tmp", "unit")

func clearCache(t *testing.T) {
	if err := os.RemoveAll(testDir); err != nil {
		t.Fatal(err)
	}
}

func TestFSStore_ReadsWritesReads(t *testing.T) {
	clearCache(t)

	store := newFSStore(testDir)

	_, ok, err := store.Read(testKey)
	if err != nil {
		t.Fatal(err)
	}
	assert.Assert(t, !ok)

	testStr := time.Now().String()
	if err := store.Write(testKey, ([]byte)(testStr)); err != nil {
		t.Fatal(err)
	}

	retrieved, ok, err := store.Read(testKey)
	if err != nil {
		t.Fatal(err)
	}
	assert.Assert(t, ok)
	assert.Equal(t, (string)(retrieved), testStr, "Data don't match")
}

func TestFSStore_OverwriteData(t *testing.T) {
	clearCache(t)

	store := newFSStore(testDir)

	testStr1 := time.Now().String() + "data1"
	if err := store.Write(testKey, ([]byte)(testStr1)); err != nil {
		t.Fatal(err)
	}

	retrieved, ok, err := store.Read(testKey)
	if err != nil {
		t.Fatal(err)
	}
	assert.Assert(t, ok)
	assert.Equal(t, (string)(retrieved), testStr1, "Data don't match")

	testStr2 := time.Now().String() + "data2"
	if err := store.Write(testKey, ([]byte)(testStr2)); err != nil {
		t.Fatal(err)
	}

	retrieved, ok, err = store.Read(testKey)
	if err != nil {
		t.Fatal(err)
	}
	assert.Assert(t, ok)
	assert.Equal(t, (string)(retrieved), testStr2, "Data don't match")
}
