package devicedb

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/api"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/errors"
)

const dbAPIVersion = 5

var useViews = true

var visibilityMap = map[int]string{
	0: "always",
	1: "advanced",
	2: "hardware_modification",
}

func init() {
	if os.Getenv("ZABER_DEVICE_DB_BYPASS_VIEWS") == "1" {
		useViews = false
	}
}

var forbiddenTablesRegexp = regexp.MustCompile(`(?i)(Data|Console)_\w+`)

// Processes every query.
// Allows us to bypass views if we want to.
func q(query string) string {
	if os.Getenv("ZABER_DEVICE_DB_TEST") == "1" {
		if (forbiddenTablesRegexp.MatchString(query)) {
			panic("The queries must strictly use the SwWeb views. Use ZABER_DEVICE_DB_BYPASS_VIEWS environment variable for development.")
		}
	}

	if !useViews {
		query = strings.ReplaceAll(query, "SwWeb_", "Data_")
	}
	return query
}

func checkCompatible(db *sql.DB) errors.Error {
	if !useViews {
		return nil
	}

	rows, err := db.Query(q(`SELECT Version FROM SwWeb_Version;`))

	if err != nil {
		return errors.Wrap(err)
	}
	defer rows.Close()

	version := -1
	if rows.Next() {
		if err := rows.Scan(&version); err != nil {
			return errors.Wrap(err)
		}
	}

	if err := rows.Err(); err != nil {
		return errors.Wrap(err)
	}

	if dbAPIVersion != version {
		msg := fmt.Sprintf("Provided device database is not compatible (have version %d, need %d).", version, dbAPIVersion)
		return errors.NewType(errors.ErrorIncompatibleDatabase, msg)
	}

	return nil
}

func (db *deviceDB) GetProduct(deviceID string, fw *dto.FirmwareVersion) (*dto.Device, errors.Error) {
	sql, err := db.DB()
	if err != nil {
		return nil, err
	}
	return getProduct(sql, deviceID, fw)
}

func getProduct(db *sql.DB, deviceID string, fw *dto.FirmwareVersion) (*dto.Device, errors.Error) {
	var build int
	var versionFilter string
	if fw == nil {
		versionFilter += " AND (MinorVersion < 95 OR MinorVersion = 98)"
	} else {
		if fw.Major >= 0 {
			versionFilter += fmt.Sprintf(" AND MajorVersion = %d", fw.Major)
		}
		if fw.Minor >= 0 {
			versionFilter += fmt.Sprintf(" AND MinorVersion = %d", fw.Minor)
		}
		build = fw.Build
	}

	rows, err := db.Query(q(`
	SELECT Id, Name, DevPeriId, COALESCE(MajorVersion, 0), COALESCE(MinorVersion, 0), COALESCE(Build, 0)
	FROM SwWeb_Products
	WHERE DevPeriId=? `+versionFilter+`
	ORDER BY Build=? DESC, MajorVersion DESC, MinorVersion < 95 DESC, MinorVersion DESC, Build DESC
	LIMIT 1;
	`), deviceID, build)

	if err != nil {
		return nil, errors.Wrap(err)
	}
	defer rows.Close()

	device := &dto.Device{
		Fw: &dto.FirmwareVersion{},
	}

	if !rows.Next() {
		err = rows.Err()
		if err != nil {
			return nil, errors.Wrap(err)
		}
		return nil, errors.NewType(errors.ErrorNoRecordFound,
			fmt.Sprintf("Cannot find product with device ID %s and firmware version %s", deviceID, fw),
		)
	}
	err = rows.Scan(
		&device.ProductID, &device.Name, &device.DeviceID,
		&device.Fw.Major, &device.Fw.Minor, &device.Fw.Build,
	)
	if err != nil {
		return nil, errors.Wrap(err)
	}
	err = rows.Err()
	return device, errors.Wrap(err)
}

func (db *deviceDB) GetPeripheralProduct(peripheralID string, parentProductID int) (*dto.Device, errors.Error) {
	sql, err := db.DB()
	if err != nil {
		return nil, err
	}
	return getPeripheralProduct(sql, peripheralID, parentProductID)
}

func getPeripheralProduct(db *sql.DB, peripheralID string, parentProductID int) (*dto.Device, errors.Error) {
	rows, err := db.Query(q(`
	SELECT Id, Name, DevPeriId
	FROM SwWeb_Products
	WHERE DevPeriId=? AND ParentId=?
	LIMIT 1;
	`), peripheralID, parentProductID)
	if err != nil {
		return nil, errors.Wrap(err)
	}
	defer rows.Close()

	device := &dto.Device{}

	if !rows.Next() {
		err = rows.Err()
		if err != nil {
			return nil, errors.Wrap(err)
		}
		return nil, errors.NewType(errors.ErrorNoRecordFound,
			fmt.Sprintf("Cannot find product with peripheral ID %s and parent product ID %d",
				peripheralID, parentProductID),
		)
	}
	err = rows.Scan(&device.ProductID, &device.Name, &device.DeviceID)
	if err != nil {
		return nil, errors.Wrap(err)
	}
	err = rows.Err()
	return device, errors.Wrap(err)
}

func (db *deviceDB) GetSaveRestoreVersion(productID int) (int, errors.Error) {
	sql, err := db.DB()
	if err != nil {
		return 0, err
	}
	return getSaveRestoreVersion(sql, productID)
}

func getSaveRestoreVersion(db *sql.DB, productID int) (int, errors.Error) {
	rows, err := db.Query(q(`
	SELECT COALESCE(NVSchemaVersion, 0)
	FROM SwWeb_Products
	WHERE Id = ?;
	`), productID)

	if err != nil {
		return 0, errors.Wrap(err)
	}
	defer rows.Close()

	if !rows.Next() {
		err = rows.Err()
		if err != nil {
			return 0, errors.Wrap(err)
		}
		return 0, errors.NewType(errors.ErrorNoRecordFound,
			fmt.Sprintf("Cannot find product with product ID %d", productID),
		)
	}

	var saveStateVersion int
	err = rows.Scan(&saveStateVersion)
	if err != nil {
		return 0, errors.Wrap(err)
	}
	err = rows.Err()
	return saveStateVersion, errors.Wrap(err)
}

func (db *deviceDB) GetConversionTable(productID int) (*dto.ConversionTable, errors.Error) {
	sql, err := db.DB()
	if err != nil {
		return nil, err
	}
	return getConversionTable(sql, productID)
}

func getConversionTable(db *sql.DB, productID int) (*dto.ConversionTable, errors.Error) {
	rows, err := db.Query(q(`
SELECT SwWeb_ContextualDimensions.Id, SwWeb_ContextualDimensions.Name, SwWeb_Dimensions.DimensionName, SwWeb_UnitFunctions.Name, Scale
FROM SwWeb_ProductsCtxDimensionsFunctions
JOIN SwWeb_ContextualDimensions ON (SwWeb_ProductsCtxDimensionsFunctions.ContextualDimensionId = SwWeb_ContextualDimensions.Id)
JOIN SwWeb_Dimensions ON (SwWeb_ContextualDimensions.DimensionId = SwWeb_Dimensions.Id)
JOIN SwWeb_UnitFunctions ON (SwWeb_ProductsCtxDimensionsFunctions.FunctionId = SwWeb_UnitFunctions.Id)
WHERE productId = ?;
	`), productID)
	if err != nil {
		return nil, errors.Wrap(err)
	}
	defer rows.Close()

	table := &dto.ConversionTable{
		Rows: make([]*dto.ConversionTableRow, 0),
	}

	for rows.Next() {
		row := &dto.ConversionTableRow{}
		err = rows.Scan(&row.ContextualDimensionID, &row.ContextualDimensionName, &row.DimensionName, &row.FunctionName, &row.Scale)
		if err != nil {
			return nil, errors.Wrap(err)
		}
		table.Rows = append(table.Rows, row)
	}
	return table, errors.Wrap(rows.Err())
}

func (db *deviceDB) GetCommandTree(productID int, mod Modified) (*dto.CommandNode, errors.Error) {
	sql, err := db.DB()
	if err != nil {
		return nil, err
	}
	return getCommandTree(sql, productID, mod)
}

type commandNodeWithID struct {
	id       int
	parentID sql.NullInt64
	enumType sql.NullInt64
	node     *dto.CommandNode
}

func getCommandTree(db *sql.DB, productID int, mod Modified) (*dto.CommandNode, errors.Error) {
	rows, err := db.Query(q(`
WITH parents
	AS (SELECT
			SwWeb_ASCIICommands.Id,
			SwWeb_ASCIICommands.ParentId
		FROM SwWeb_ASCIICommands
		JOIN SwWeb_ProductsASCIICommands ON (SwWeb_ASCIICommands.Id = SwWeb_ProductsASCIICommands.ASCIICommandId)
		WHERE ProductId = ? AND `+getModRestrict(mod)+`
		UNION
		SELECT
			SwWeb_ASCIICommands.Id,
			SwWeb_ASCIICommands.ParentId
		FROM SwWeb_ASCIICommands, parents
		WHERE SwWeb_ASCIICommands.Id = parents.ParentId)
SELECT
	cmd.Id,
	cmd.ParentId,
	cmd.Command,
	cmd.ParamType IS NOT NULL as IsParam,
	cmd.HasCallback as IsCommand,
	COALESCE(cmd.ContextualDimension, 0) as CtxDimensionId,
	COALESCE(SwWeb_ParamTypes.Name, ''),
	COALESCE(cmd.Arity, -1) as Arity,
	cmd.EnumType
FROM parents
JOIN SwWeb_ASCIICommands cmd ON (cmd.Id = parents.Id)
LEFT JOIN SwWeb_ParamTypes ON (cmd.ParamType = SwWeb_ParamTypes.Id);
`), productID)
	if err != nil {
		return nil, errors.Wrap(err)
	}
	defer rows.Close()

	root := &dto.CommandNode{}
	nodes := make(map[int]*commandNodeWithID)
	enumTypesToQuery := make(enumIDsSet)

	for rows.Next() {
		var id int
		var parentID sql.NullInt64
		var enumType sql.NullInt64
		node := &dto.CommandNode{}

		err = rows.Scan(&id, &parentID, &node.Command, &node.IsParam, &node.IsCommand,
			&node.ContextualDimensionID, &node.ParamType, &node.ParamArity, &enumType)
		if err != nil {
			return nil, errors.Wrap(err)
		}
		if !node.IsParam {
			node.ParamArity = 0
		}
		nodes[id] = &commandNodeWithID{
			id:       id,
			parentID: parentID,
			node:     node,
			enumType: enumType,
		}

		if enumType.Valid {
			enumTypesToQuery[(int)(enumType.Int64)] = true
		}
	}

	for _, node := range nodes {
		if node.parentID.Valid {
			parent := nodes[(int)(node.parentID.Int64)]
			parent.node.Nodes = append(parent.node.Nodes, node.node)
		} else {
			root.Nodes = append(root.Nodes, node.node)
		}
	}

	if len(enumTypesToQuery) > 0 {
		enumValues, err := getEnumValues(db, enumTypesToQuery)
		if err != nil {
			return nil, err
		}

		for _, node := range nodes {
			if node.enumType.Valid {
				node.node.Values = enumValues[(int)(node.enumType.Int64)]
			}
		}

	}

	return root, errors.Wrap(rows.Err())
}

type enumValuesMap = map[int][]string
type enumIDsSet = map[int]bool

func getEnumValues(db *sql.DB, enumTypes enumIDsSet) (enumValuesMap, errors.Error) {
	var typeIDs strings.Builder
	for typeID := range enumTypes {
		if typeIDs.Len() > 0 {
			typeIDs.WriteString(",")
		}
		typeIDs.WriteString(strconv.Itoa(typeID))
	}

	rows, err := db.Query(q(fmt.Sprintf(`
SELECT TypeId, Name
FROM SwWeb_EnumNodes
WHERE TypeId IN (%s);
`, typeIDs.String())))

	if err != nil {
		return nil, errors.Wrap(err)
	}
	defer rows.Close()

	valuesMap := make(enumValuesMap)

	for rows.Next() {
		var id int
		var name string

		err = rows.Scan(&id, &name)
		if err != nil {
			return nil, errors.Wrap(err)
		}

		valuesMap[id] = append(valuesMap[id], name)
	}

	return valuesMap, errors.Wrap(rows.Err())
}

func (db *deviceDB) GetSettingsTable(productID int, mod Modified) (*dto.SettingsTable, errors.Error) {
	sql, err := db.DB()
	if err != nil {
		return nil, err
	}
	return getSettingsTable(sql, productID, mod)
}

func getSettingsTable(db *sql.DB, productID int, mod Modified) (*dto.SettingsTable, errors.Error) {
	rows, err := db.Query(q(`
SELECT ASCIIName, COALESCE(ContextualDimension, 0), SwWeb_ParamTypes.Name, SwWeb_ParamTypes.DecimalPlaces, COALESCE(ASCIIReadLevel, 0), COALESCE(ASCIIWriteLevel, 0), CAST(Visibility AS INT), DefaultValue, IsRealTimeSafe
FROM SwWeb_Settings
JOIN SwWeb_ProductsSettings ON (SwWeb_ProductsSettings.SettingId = SwWeb_Settings.Id)
JOIN SwWeb_ParamTypes ON (SwWeb_Settings.ParamType = SwWeb_ParamTypes.Id)
WHERE ASCIIName IS NOT NULL AND ProductId = ? AND `+getModRestrict(mod)+`;
	`), productID)
	if err != nil {
		return nil, errors.Wrap(err)
	}
	defer rows.Close()

	table := &dto.SettingsTable{
		Rows: make([]*dto.SettingsTableRow, 0),
	}

	for rows.Next() {
		var visibilityNum int
		row := &dto.SettingsTableRow{}
		err = rows.Scan(&row.Name, &row.ContextualDimensionID, &row.ParamType, &row.DecimalPlaces, &row.ReadAccessLevel, &row.WriteAccessLevel, &visibilityNum, &row.DefaultValue, &row.Realtime)
		if err != nil {
			return nil, errors.Wrap(err)
		}
		row.Visibility = visibilityMap[visibilityNum]
		table.Rows = append(table.Rows, row)
	}
	return table, errors.Wrap(rows.Err())
}

func (db *deviceDB) GetBinarySettingsTable(productID int) (*dto.BinarySettingsTable, errors.Error) {
	sql, err := db.DB()
	if err != nil {
		return nil, err
	}
	return getBinarySettingsTable(sql, productID)
}

func getBinarySettingsTable(db *sql.DB, productID int) (*dto.BinarySettingsTable, errors.Error) {
	rows, err := db.Query(q(`
	SELECT BinaryName, COALESCE(ContextualDimension, 0), COALESCE(BinaryReturnCommand, 0), COALESCE(BinarySetCommand, 0)
	FROM SwWeb_Settings
	JOIN SwWeb_ProductsSettings ON (SwWeb_ProductsSettings.SettingId = SwWeb_Settings.Id)
	WHERE BinaryName IS NOT NULL AND ProductId = ?;
	`), productID)
	if err != nil {
		return nil, errors.Wrap(err)
	}
	defer rows.Close()

	table := &dto.BinarySettingsTable{
		Rows: make([]*dto.BinarySettingsTableRow, 0),
	}

	for rows.Next() {
		row := &dto.BinarySettingsTableRow{}
		err = rows.Scan(&row.Name, &row.ContextualDimensionID, &row.GetCommand, &row.SetCommand)
		if err != nil {
			return nil, errors.Wrap(err)
		}
		table.Rows = append(table.Rows, row)
	}
	return table, errors.Wrap(rows.Err())
}

func (db *deviceDB) GetBinaryCommandsTable(productID int) (*dto.BinaryCommandsTable, errors.Error) {
	sql, err := db.DB()
	if err != nil {
		return nil, err
	}
	return getBinaryCommandsTable(sql, productID)
}

func getBinaryCommandsTable(db *sql.DB, productID int) (*dto.BinaryCommandsTable, errors.Error) {
	rows, err := db.Query(q(`
	SELECT Command, Name, Parameter > 0, COALESCE(ParameterContextualDimension, 0), COALESCE(ResponseContextualDimension, 0)
	FROM SwWeb_BinaryCommands
	JOIN SwWeb_ProductsBinaryCommands ON (SwWeb_ProductsBinaryCommands.BinaryCommandId = SwWeb_BinaryCommands.Id)
	WHERE SwWeb_ProductsBinaryCommands.ProductId = ?;
	`), productID)
	if err != nil {
		return nil, errors.Wrap(err)
	}
	defer rows.Close()

	table := &dto.BinaryCommandsTable{
		Rows: make([]*dto.BinaryCommandsTableRow, 0),
	}

	for rows.Next() {
		row := &dto.BinaryCommandsTableRow{}
		err = rows.Scan(&row.Command, &row.Name, &row.HasParam, &row.ParamContextualDimensionID, &row.ReturnContextualDimensionID)
		if err != nil {
			return nil, errors.Wrap(err)
		}
		table.Rows = append(table.Rows, row)
	}
	return table, errors.Wrap(rows.Err())
}

func (db *deviceDB) GetBinaryRepliesTable(productID int) (*dto.BinaryRepliesTable, errors.Error) {
	sql, err := db.DB()
	if err != nil {
		return nil, err
	}
	return getBinaryRepliesTable(sql, productID)
}

func getBinaryRepliesTable(db *sql.DB, productID int) (*dto.BinaryRepliesTable, errors.Error) {
	rows, err := db.Query(q(`
	SELECT Reply, Name, COALESCE(ContextualDimension, 0)
	FROM SwWeb_BinaryReplies
	JOIN SwWeb_ProductsBinaryReplies ON (SwWeb_ProductsBinaryReplies.BinaryReplyId = SwWeb_BinaryReplies.Id)
	WHERE SwWeb_ProductsBinaryReplies.ProductId = ?;
	`), productID)
	if err != nil {
		return nil, errors.Wrap(err)
	}
	defer rows.Close()

	table := &dto.BinaryRepliesTable{
		Rows: make([]*dto.BinaryRepliesTableRow, 0),
	}

	for rows.Next() {
		row := &dto.BinaryRepliesTableRow{}
		err = rows.Scan(&row.Reply, &row.Name, &row.ContextualDimensionID)
		if err != nil {
			return nil, errors.Wrap(err)
		}
		table.Rows = append(table.Rows, row)
	}
	return table, errors.Wrap(rows.Err())
}

func (db *deviceDB) GetCapabilities(productID int, mod Modified) ([]string, errors.Error) {
	sql, err := db.DB()
	if err != nil {
		return nil, err
	}
	return getCapabilities(sql, productID, mod)
}

func getCapabilities(db *sql.DB, productID int, mod Modified) ([]string, errors.Error) {
	rows, err := db.Query(q(`
	SELECT Capability
	FROM SwWeb_ProductsCapabilities
	JOIN SwWeb_Capabilities ON (SwWeb_Capabilities.Id = SwWeb_ProductsCapabilities.CapabilityId)
	WHERE ProductId = ? AND `+getModRestrict(mod)+`;
	`), productID)
	if err != nil {
		return nil, errors.Wrap(err)
	}
	defer rows.Close()

	capabilities := make([]string, 0)

	for rows.Next() {
		var capability string
		err = rows.Scan(&capability)
		if err != nil {
			return nil, errors.Wrap(err)
		}
		capabilities = append(capabilities, capability)
	}
	return capabilities, errors.Wrap(rows.Err())
}

func (db *deviceDB) GetSupportedPeripherals(productID int) ([]*dto.SupportedPeripheral, errors.Error) {
	sql, err := db.DB()
	if err != nil {
		return nil, err
	}
	return getSupportedPeripherals(sql, productID)
}

func getSupportedPeripherals(db *sql.DB, productID int) ([]*dto.SupportedPeripheral, errors.Error) {
	rows, err := db.Query(q(`
	SELECT Id, DevPeriId, Name FROM SwWeb_Products WHERE ParentId = ?;
	`), productID)
	if err != nil {
		return nil, errors.Wrap(err)
	}
	defer rows.Close()

	peripherals := make([]*dto.SupportedPeripheral, 0)

	for rows.Next() {
		peripheral := &dto.SupportedPeripheral{}
		err = rows.Scan(&peripheral.ProductID, &peripheral.PeripheralID, &peripheral.Name)
		if err != nil {
			return nil, errors.Wrap(err)
		}
		peripherals = append(peripherals, peripheral)
	}

	if len(peripherals) == 0 && rows.Err() == nil {
		return nil, errors.NewType(errors.ErrorNoRecordFound,
			fmt.Sprintf("Product with product ID %d does not exist or support peripherals", productID),
		)
	}

	return peripherals, errors.Wrap(rows.Err())
}

func getModRestrict(mod Modified) string {
	modRestrict := 0
	if mod.Self {
		modRestrict = 1
	}
	parentModRestrict := 0
	if mod.Parent {
		parentModRestrict = 1
	}
	return fmt.Sprintf(
		"((ModRestrict IS NULL OR ModRestrict = %d) AND (ParentModRestrict IS NULL OR ParentModRestrict = %d))",
		modRestrict, parentModRestrict)
}

func (db *deviceDB) GetDefaultSettings(productID int) (map[string]string, errors.Error) {
	sql, err := db.DB()
	if err != nil {
		return nil, err
	}
	return getDefaultSettings(sql, productID)
}

func getDefaultSettings(db *sql.DB, productID int) (map[string]string, errors.Error) {
	rows, err := db.Query(q(`
		SELECT s.ASCIIName as Name, ps.DefaultValue as DefaultValue
		FROM SwWeb_ProductsSettings ps
		JOIN SwWeb_Settings s ON ps.SettingId = s.id
		WHERE ps.ProductId = ? AND s.ASCIIName NOT NULL AND ps.DefaultValue NOT NULL;`), productID)

	if err != nil {
		return nil, errors.Wrap(err)
	}
	defer rows.Close()

	values := make(map[string]string)

	for rows.Next() {
		var name string
		var value string
		err = rows.Scan(&name, &value)
		if err != nil {
			return nil, errors.Wrap(err)
		}

		values[name] = value
	}

	return values, errors.Wrap(rows.Err())
}

func (db *deviceDB) GetServoTuningInfo(productID int) (*dto.ServoTuningInfo, errors.Error) {
	sql, err := db.DB()
	if err != nil {
		return nil, err
	}
	return getServoTuningInfo(sql, productID)
}

func getServoTuningInfo(db *sql.DB, productID int) (*dto.ServoTuningInfo, errors.Error) {
	notFoundErr := errors.NewType(errors.ErrorNoRecordFound,
		fmt.Sprintf("Product with product ID %d does not exist or does not support servo tuning", productID),
	)

	servoControllerRow := db.QueryRow(q(`
		SELECT
			SwWeb_ServoControllers.Name, SwWeb_ServoControllers.Version
		FROM SwWeb_ProductsServoControllers JOIN SwWeb_ServoControllers
			ON SwWeb_ProductsServoControllers.ServoControllerId = SwWeb_ServoControllers.Id
		WHERE SwWeb_ProductsServoControllers.ProductId = ?
	`), productID)

	var algorithm string
	var algoVer int

	err := servoControllerRow.Scan(&algorithm, &algoVer)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, notFoundErr
		}
		return nil, errors.Wrap(err)
	}

	servoTuningRow := db.QueryRow(q(`
		SELECT
			SwWeb_Products.ForceOrTorqueConstant,
			SwWeb_ServoTuning.TunerGuidanceData,
			SwWeb_Products.CarriageInertia,
			SwWeb_Products.MotorInertia,
			SwWeb_Products.DriveRatio
		FROM SwWeb_Products LEFT JOIN SwWeb_ServoTuning
			ON SwWeb_Products.ServoTuningDataId = SwWeb_ServoTuning.Id
		WHERE SwWeb_Products.Id = ?
	`), productID)

	var forque float64
	var marshalledGuidance string
	var carriageInertiaGrams float64
	var driveRatio float64
	var motorInertia float64

	err = servoTuningRow.Scan(&forque, &marshalledGuidance, &carriageInertiaGrams, &motorInertia, &driveRatio)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, notFoundErr
		}
		return nil, errors.Wrap(err)
	}

	var guidanceData dto.ServoTuningSimpleGuidanceEncoded
	if err := json.Unmarshal([]byte(marshalledGuidance), &guidanceData); err != nil {
		return nil, errors.Wrap(err)
	}

	return &dto.ServoTuningInfo{
		Algorithm:        algorithm,
		AlgorithmVersion: algoVer,
		Forque:           forque,
		CarriageInertia:  carriageInertiaGrams,
		DriveRatio:       driveRatio,
		MotorInertia:     motorInertia,
		Guidance:         guidanceData.GuidanceData,
	}, nil
}

func (db *deviceDB) SearchProduct(query *api.ProductSearchQuery) (*dto.FoundProducts, errors.Error) {
	sqlInstance, dbErr := db.DB()
	if dbErr != nil {
		return nil, dbErr
	}

	var versionFilter string
	if query.FW != nil {
		if query.FW.Major >= 0 {
			versionFilter += fmt.Sprintf(" AND COALESCE(prod.MajorVersion, parent.MajorVersion) = %d", query.FW.Major)
		}
		if query.FW.Minor >= 0 {
			versionFilter += fmt.Sprintf(" AND COALESCE(prod.MinorVersion, parent.MinorVersion) = %d", query.FW.Minor)
		}
		if query.FW.Build >= 0 {
			versionFilter += fmt.Sprintf(" AND COALESCE(prod.Build, parent.Build) = %d", query.FW.Build)
		}
	}

	if query.FW == nil || query.FW.Minor != 99 {
		versionFilter += " AND COALESCE(prod.MinorVersion, parent.MinorVersion) != 99"
	}

	var rows *sql.Rows
	var err error
	if query.DeviceID != "" {
		rows, err = sqlInstance.Query(q(`
SELECT prod.Name, prod.DevPeriId, prod.ParentId IS NULL AS isDevice
FROM SwWeb_Products prod
LEFT JOIN SwWeb_Products parent ON prod.ParentId = parent.Id
WHERE prod.DevPeriId=? `+versionFilter+`
ORDER BY
	COALESCE(prod.MajorVersion, parent.MajorVersion) DESC,
	COALESCE(prod.MinorVersion, parent.MinorVersion) DESC,
	COALESCE(prod.Build, parent.Build) DESC
LIMIT 1;
		`), query.DeviceID)
	} else {
		rows, err = sqlInstance.Query(q(`
SELECT top.Name, top.DevPeriId, top.ParentId IS NULL AS isDevice FROM (
	SELECT prod.*,
	RANK () OVER(
		PARTITION BY prod.DevPeriId
		ORDER BY prod.ParentId, COALESCE(prod.Build, parent.Build) DESC
	) AS ProductRank
	FROM SwWeb_Products prod
	LEFT JOIN SwWeb_Products parent ON prod.ParentId = parent.Id
	WHERE prod.Name LIKE ? `+versionFilter+`
	AND prod.DevPeriId != 0
) top
WHERE top.ProductRank = 1
ORDER BY top.name
LIMIT ?;
		`), query.Name, query.Limit)
	}

	if err != nil {
		return nil, errors.Wrap(err)
	}
	defer rows.Close()

	products := make([]*dto.FoundProduct, 0)

	for rows.Next() {
		product := &dto.FoundProduct{}
		var isDevice bool
		var id int
		err = rows.Scan(&product.Name, &id, &isDevice)
		if err != nil {
			return nil, errors.Wrap(err)
		}

		if isDevice {
			product.DeviceID = id
		} else {
			product.PeripheralID = id
		}

		products = append(products, product)
	}

	return &dto.FoundProducts{
		Products: products,
	}, errors.Wrap(rows.Err())
}
