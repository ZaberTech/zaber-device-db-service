package devicedb

import (
	"crypto/md5"
	"database/sql"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"sync"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/api"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/errors"

	_ "github.com/mattn/go-sqlite3" // database driver
)

type Modified struct {
	Self   bool
	Parent bool
}

type DeviceDB interface {
	CheckDB() errors.Error
	Close() errors.Error
	GetProduct(deviceID string, fw *dto.FirmwareVersion) (*dto.Device, errors.Error)
	GetPeripheralProduct(peripheralID string, parentProductID int) (*dto.Device, errors.Error)
	GetConversionTable(productID int) (*dto.ConversionTable, errors.Error)
	GetCommandTree(productID int, mod Modified) (*dto.CommandNode, errors.Error)
	GetSettingsTable(productID int, mod Modified) (*dto.SettingsTable, errors.Error)
	GetBinarySettingsTable(productID int) (*dto.BinarySettingsTable, errors.Error)
	GetBinaryCommandsTable(productID int) (*dto.BinaryCommandsTable, errors.Error)
	GetBinaryRepliesTable(productID int) (*dto.BinaryRepliesTable, errors.Error)
	GetCapabilities(productID int, mod Modified) ([]string, errors.Error)
	GetSupportedPeripherals(productID int) ([]*dto.SupportedPeripheral, errors.Error)
	GetDefaultSettings(productID int) (map[string]string, errors.Error)
	GetServoTuningInfo(productID int) (*dto.ServoTuningInfo, errors.Error)
	GetSaveRestoreVersion(productID int) (int, errors.Error)
	GetFileChecksum() (string, errors.Error)
	SearchProduct(query *api.ProductSearchQuery) (*dto.FoundProducts, errors.Error)
}

type deviceDB struct {
	filePath string

	openOnce sync.Once
	openErr  errors.Error
	db       *sql.DB

	checksum     string
	checksumErr  errors.Error
	checksumLock sync.Mutex
}

func NewDeviceDB(dbFilePath string) DeviceDB {
	db := &deviceDB{
		filePath:    dbFilePath,
		checksumErr: errors.NewType(errors.ErrorChecksumNotAvailableYet, "Checksum is not calculated yet"),
	}
	return db
}

func (db *deviceDB) open(dbFilePath string) (*sql.DB, errors.Error) {
	openedDb, err := sql.Open(driver, fmt.Sprintf("file:%s?mode=ro", dbFilePath))
	if err != nil {
		return nil, errors.New("Cannot open the database: " + err.Error())
	}

	if err := checkCompatible(openedDb); err != nil {
		return nil, err
	}

	return openedDb, nil
}

func (db *deviceDB) ensureOpen() errors.Error {
	db.openOnce.Do(func() {
		db.db, db.openErr = db.open(db.filePath)
		go db.calculateChecksum()
	})
	if db.openErr != nil {
		return db.openErr
	}
	return nil
}

func (db *deviceDB) DB() (*sql.DB, errors.Error) {
	if err := db.ensureOpen(); err != nil {
		return nil, err
	}
	return db.db, nil
}

func (db *deviceDB) CheckDB() errors.Error {
	return db.ensureOpen()
}

func (db *deviceDB) GetFileChecksum() (string, errors.Error) {
	if err := db.ensureOpen(); err != nil {
		return "", err
	}
	db.checksumLock.Lock()
	defer db.checksumLock.Unlock()
	return db.checksum, db.checksumErr
}

func (db *deviceDB) calculateChecksum() {
	checksum, err := calculateChecksum(db.filePath)

	db.checksumLock.Lock()
	defer db.checksumLock.Unlock()
	db.checksum = checksum
	db.checksumErr = err
}

func calculateChecksum(filePath string) (string, errors.Error) {
	file, err := os.Open(filePath)
	if err != nil {
		return "", errors.New("Cannot open the database: " + err.Error())
	}
	defer file.Close()

	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		return "", errors.New("Failed to read database file: " + err.Error())
	}
	hashBytes := hash.Sum(nil)
	hashString := hex.EncodeToString(hashBytes)

	return hashString, nil
}

func (db *deviceDB) Close() errors.Error {
	sql, err := db.DB()
	if err != nil {
		// if cannot be opened, ignore
		return nil
	}
	if err := sql.Close(); err != nil {
		return errors.New("Cannot close the database: " + err.Error())
	}
	return nil
}
