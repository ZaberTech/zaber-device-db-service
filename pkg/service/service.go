package service

import (
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/api"
	devicedb "gitlab.com/ZaberTech/zaber-device-db-service/pkg/device_db"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/errors"
)

type service struct {
	deviceDB devicedb.DeviceDB
}

type FileService interface {
	api.BaseService
	GetFileChecksum() (string, errors.Error)
	GetHealth() errors.Error
	Close() errors.Error
}

func NewService(dbFilePath string) FileService {
	instance := &service{
		deviceDB: devicedb.NewDeviceDB(dbFilePath),
	}

	return instance
}

func (instance *service) GetProductInformation(query *api.GetProductInformationQuery) (*dto.DeviceInfo, errors.Error) {
	deviceMod := devicedb.Modified{
		Self: query.Modified,
	}
	device, err := instance.deviceDB.GetProduct(query.DeviceID, query.FW)
	if err != nil {
		return nil, err
	}
	conversionTable, err := instance.deviceDB.GetConversionTable(device.ProductID)
	if err != nil {
		return nil, err
	}
	commandTree, err := instance.deviceDB.GetCommandTree(device.ProductID, deviceMod)
	if err != nil {
		return nil, err
	}
	settings, err := instance.deviceDB.GetSettingsTable(device.ProductID, deviceMod)
	if err != nil {
		return nil, err
	}
	capabilities, err := instance.deviceDB.GetCapabilities(device.ProductID, deviceMod)
	if err != nil {
		return nil, err
	}
	saveStateVersion, err := instance.deviceDB.GetSaveRestoreVersion(device.ProductID)
	if err != nil {
		return nil, err
	}

	info := &dto.DeviceInfo{
		Device:           device,
		ConversionTable:  conversionTable,
		CommandTree:      commandTree,
		Settings:         settings,
		Capabilities:     capabilities,
		SaveStateVersion: saveStateVersion,
	}

	if len(query.Peripherals) > 0 {
		info.Peripherals = make([]*dto.Peripheral, len(query.Peripherals))
	}
	for i, peripheral := range query.Peripherals {
		peripheralMod := devicedb.Modified{
			Self:   peripheral.Modified,
			Parent: deviceMod.Self,
		}
		peripheralDevice, err := instance.deviceDB.GetPeripheralProduct(peripheral.PeripheralID, device.ProductID)
		if err != nil {
			return nil, err
		}
		conversionTable, err := instance.deviceDB.GetConversionTable(peripheralDevice.ProductID)
		if err != nil {
			return nil, err
		}
		commandTree, err := instance.deviceDB.GetCommandTree(peripheralDevice.ProductID, peripheralMod)
		if err != nil {
			return nil, err
		}
		settings, err := instance.deviceDB.GetSettingsTable(peripheralDevice.ProductID, peripheralMod)
		if err != nil {
			return nil, err
		}
		capabilities, err := instance.deviceDB.GetCapabilities(peripheralDevice.ProductID, peripheralMod)
		if err != nil {
			return nil, err
		}

		info.Peripherals[i] = &dto.Peripheral{
			Device:          peripheralDevice,
			ConversionTable: conversionTable,
			CommandTree:     commandTree,
			Settings:        settings,
			Capabilities:    capabilities,
		}
	}
	return info, nil
}

func (instance *service) GetBinaryProductInformation(deviceID string, fw *dto.FirmwareVersion, peripheralID string) (*dto.BinaryDeviceInfo, errors.Error) {
	device, err := instance.deviceDB.GetProduct(deviceID, fw)
	if err != nil {
		return nil, err
	}
	conversionTable, err := instance.deviceDB.GetConversionTable(device.ProductID)
	if err != nil {
		return nil, err
	}
	commands, err := instance.deviceDB.GetBinaryCommandsTable(device.ProductID)
	if err != nil {
		return nil, err
	}
	replies, err := instance.deviceDB.GetBinaryRepliesTable(device.ProductID)
	if err != nil {
		return nil, err
	}
	settings, err := instance.deviceDB.GetBinarySettingsTable(device.ProductID)
	if err != nil {
		return nil, err
	}
	capabilities, err := instance.deviceDB.GetCapabilities(device.ProductID, devicedb.Modified{})
	if err != nil {
		return nil, err
	}

	info := &dto.BinaryDeviceInfo{
		Device:          device,
		ConversionTable: conversionTable,
		Commands:        commands,
		Replies:         replies,
		Settings:        settings,
		Capabilities:    capabilities,
	}

	if len(peripheralID) > 0 {
		peripheralDevice, err := instance.deviceDB.GetPeripheralProduct(peripheralID, device.ProductID)
		if err != nil {
			return nil, err
		}
		conversionTable, err := instance.deviceDB.GetConversionTable(peripheralDevice.ProductID)
		if err != nil {
			return nil, err
		}
		commands, err := instance.deviceDB.GetBinaryCommandsTable(peripheralDevice.ProductID)
		if err != nil {
			return nil, err
		}
		replies, err := instance.deviceDB.GetBinaryRepliesTable(peripheralDevice.ProductID)
		if err != nil {
			return nil, err
		}
		settings, err := instance.deviceDB.GetBinarySettingsTable(peripheralDevice.ProductID)
		if err != nil {
			return nil, err
		}
		capabilities, err := instance.deviceDB.GetCapabilities(peripheralDevice.ProductID, devicedb.Modified{})
		if err != nil {
			return nil, err
		}
		info.Peripheral = &dto.BinaryPeripheral{
			Device:          peripheralDevice,
			ConversionTable: conversionTable,
			Commands:        commands,
			Replies:         replies,
			Settings:        settings,
			Capabilities:    capabilities,
		}
	}
	return info, nil
}

func (instance *service) GetSupportedPeripherals(productID int) (*dto.SupportedPeripherals, errors.Error) {
	peripherals, err := instance.deviceDB.GetSupportedPeripherals(productID)
	if err != nil {
		return nil, err
	}
	return &dto.SupportedPeripherals{
		Peripherals: peripherals,
	}, nil
}

func (instance *service) GetServoTuningInfo(productID int) (*dto.ServoTuningInfo, errors.Error) {
	return instance.deviceDB.GetServoTuningInfo(productID)
}

func (instance *service) GetDefaultSettings(deviceID string, fw *dto.FirmwareVersion, peripheralIDs []string) (*dto.DefaultSettings, errors.Error) {
	device, err := instance.deviceDB.GetProduct(deviceID, fw)
	if err != nil {
		return nil, err
	}

	deviceValues, err := instance.deviceDB.GetDefaultSettings(device.ProductID)
	if err != nil {
		return nil, err
	}

	deviceResults := &dto.DefaultSettings{
		DeviceID:    device.DeviceID,
		Fw:          device.Fw,
		Defaults:    deviceValues,
		Peripherals: make([]*dto.PeripheralDefaultSettings, len(peripheralIDs)),
	}

	if len(peripheralIDs) > 0 {
		for i, pid := range peripheralIDs {
			peripheral, err := instance.deviceDB.GetPeripheralProduct(pid, device.ProductID)
			if err != nil {
				return nil, err
			}

			axisValues, err := instance.deviceDB.GetDefaultSettings(peripheral.ProductID)
			if err != nil {
				return nil, err
			}

			deviceResults.Peripherals[i] = &dto.PeripheralDefaultSettings{
				PeripheralID: peripheral.DeviceID,
				Defaults:     axisValues,
			}
		}
	}

	return deviceResults, nil
}

func (instance *service) SearchProduct(query *api.ProductSearchQuery) (*dto.FoundProducts, errors.Error) {
	return instance.deviceDB.SearchProduct(query)
}

func (instance *service) GetHealth() errors.Error {
	return instance.deviceDB.CheckDB()
}

func (instance *service) GetFileChecksum() (string, errors.Error) {
	return instance.deviceDB.GetFileChecksum()
}

func (instance *service) Close() errors.Error {
	return instance.deviceDB.Close()
}
