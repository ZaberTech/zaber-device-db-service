/* eslint-disable no-console */

const { promisify } = require('bluebird');
const fs = require('fs');
const path = require('path');
const os = require('os');
const glob = promisify(require('glob'));

const readFile = promisify(fs.readFile);
const writeFile = promisify(fs.writeFile);

const DELIMITER = '------------------';

const forkReplacements = {
  'github.com/zabertech/go-serial': 'github.com/bugst/go-serial',
};

async function collectLicenses() {
  const goSumContent = await readFile(path.join(__dirname, '..', 'go.sum'), 'utf8');

  const licenses = [];
  const noLicense = [];
  const dedup = new Set();

  for (const line of goSumContent.split('\n').filter(line => line.match(/^\w+/))) {
    const [library] = line.split(/\s+/);
    if (dedup.has(library)) {
      continue;
    }
    dedup.add(library);

    const pattern = path.join(process.env.GOPATH, 'pkg', 'mod', `${library}*`, 'LICEN[CS]E*');
    const licenseFiles = await glob(pattern);
    if (licenseFiles.length  === 0) {
      noLicense.push(library);
      continue;
    }

    const licenseFile = licenseFiles.slice(-1)[0];
    console.log(library, licenseFile);

    const content = await readFile(licenseFile, 'utf8');

    licenses.push({
      library,
      content,
    });
  }

  let finalLicense = '';

  finalLicense += await readFile(path.join(__dirname, '..', 'LICENSE')) + os.EOL;

  finalLicense +=  os.EOL + DELIMITER + os.EOL;
  finalLicense += 'The following lines contain the licenses of all libraries and tools';
  finalLicense += `that have been used to build the binary which we distribute.${os.EOL}`;

  for (const { library, content } of licenses) {
    finalLicense += os.EOL + DELIMITER + os.EOL;
    finalLicense += (forkReplacements[library] || library);
    finalLicense += os.EOL + DELIMITER + os.EOL;
    finalLicense += content;
  }

  const licensePath = path.join(__dirname, 'LICENSE.txt');
  await writeFile(licensePath, finalLicense, 'utf8');

  for (const library of noLicense) {
    console.log(`No license: ${library}`);
  }
}

module.exports = { collectLicenses };
