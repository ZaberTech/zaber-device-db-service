package testgo

import (
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"gotest.tools/v3/assert"

	"testing"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/api"
	cl "gitlab.com/ZaberTech/zaber-device-db-service/pkg/client"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/store"
)

var cacheDir = filepath.Join("..", "tmp", "client")

func modifyContent(t *testing.T, file string, modify func(oldContent string) string) {
	oldContent, err := os.ReadFile(file)
	if err != nil {
		t.Fatal(err)
	}
	newContent := modify((string)(oldContent))
	if err := os.WriteFile(file, ([]byte)(newContent), 0666); err != nil {
		t.Fatal(err)
	}
}

func readSingleFile(t *testing.T) string {
	files, errList := os.ReadDir(cacheDir)
	if errList != nil {
		t.Fatal(errList)
	}

	assert.Equal(t, len(files), 1, "Amount of files in the cache does not match")

	return filepath.Join(cacheDir, files[0].Name())
}

func TestStoreClient_WritesAndRetrievesData(t *testing.T) {
	if err := os.RemoveAll(cacheDir); err != nil {
		t.Fatal(err)
	}

	client, err := cl.NewClient(testURL, userAgent)
	if err != nil {
		t.Fatal(err)
	}

	storeClient := store.NewServiceStore(client, cacheDir)

	query := &api.GetProductInformationQuery{
		DeviceID: "30222",
	}
	_, err = storeClient.GetProductInformation(query)
	if err != nil {
		t.Fatal(err)
	}

	storeFile := readSingleFile(t)
	modifyContent(t, storeFile, func(oldContent string) string { return strings.Replace(oldContent, "X-MCB2", "Z-ABC1", -1) })

	newProductData, err := storeClient.GetProductInformation(query)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, newProductData.Device.Name, "Z-ABC1", "Does not use the stored data")
}

func TestStoreClient_PropagatesAccessReadError(t *testing.T) {
	if runtime.GOOS == "windows" {
		t.Skip("This test does not work under Windows")
	}

	if err := os.RemoveAll(cacheDir); err != nil {
		t.Fatal(err)
	}

	client, err := cl.NewClient(testURL, userAgent)
	if err != nil {
		t.Fatal(err)
	}

	storeClient := store.NewServiceStore(client, cacheDir)

	query := &api.GetProductInformationQuery{
		DeviceID: "30222",
	}

	_, err = storeClient.GetProductInformation(query)
	if err != nil {
		t.Fatal(err)
	}

	storeFile := readSingleFile(t)
	if err := os.Chmod(storeFile, 0); err != nil {
		t.Fatal(err)
	}

	defer os.Remove(storeFile)

	_, err = storeClient.GetProductInformation(query)
	assert.Assert(t, err != nil, "No error thrown")
	assert.Assert(t, strings.Contains(err.Error(), "permission denied"))
}

func TestStoreClient_PropagatesAccessWriteError(t *testing.T) {
	if runtime.GOOS == "windows" {
		t.Skip("This test does not work under Windows")
	}

	if err := os.RemoveAll(cacheDir); err != nil {
		t.Fatal(err)
	}

	// prevent writing
	if err := os.MkdirAll(cacheDir, 0555); err != nil {
		t.Fatal(err)
	}
	defer func() {
		_ = os.Chmod(cacheDir, 0777)
	}()

	client, err := cl.NewClient(testURL, userAgent)
	if err != nil {
		t.Fatal(err)
	}

	storeClient := store.NewServiceStore(client, cacheDir)

	query := &api.GetProductInformationQuery{
		DeviceID: "30222",
	}
	_, err = storeClient.GetProductInformation(query)
	assert.Assert(t, err != nil, "No error thrown")
	assert.Assert(t, strings.Contains(err.Error(), "permission denied"))
}

func TestStoreClient_IgnoresMalformedContent(t *testing.T) {
	err := os.RemoveAll(cacheDir)
	if err != nil {
		t.Fatal(err)
	}

	client, err := cl.NewClient(testURL, userAgent)
	if err != nil {
		t.Fatal(err)
	}

	storeClient := store.NewServiceStore(client, cacheDir)

	query := &api.GetProductInformationQuery{
		DeviceID: "30222",
	}
	_, err = storeClient.GetProductInformation(query)
	if err != nil {
		t.Fatal(err)
	}

	storeFile := readSingleFile(t)
	// malform content
	modifyContent(t, storeFile, func(oldContent string) string { return strings.Replace(oldContent, "\"", "", 1) })

	_, err = storeClient.GetProductInformation(query)
	if err != nil {
		t.Fatal(err)
	}
}

func TestStoreClient_WritesAndRetrievesDataForBinaryDevice(t *testing.T) {
	if err := os.RemoveAll(cacheDir); err != nil {
		t.Fatal(err)
	}

	client, err := cl.NewClient(testURL, userAgent)
	if err != nil {
		t.Fatal(err)
	}

	storeClient := store.NewServiceStore(client, cacheDir)

	_, err = storeClient.GetBinaryProductInformation("30222", nil, "")
	if err != nil {
		t.Fatal(err)
	}

	storeFile := readSingleFile(t)
	modifyContent(t, storeFile, func(oldContent string) string { return strings.Replace(oldContent, "X-MCB2", "Z-ABC1", -1) })

	newProductData, err := storeClient.GetBinaryProductInformation("30222", nil, "")
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, newProductData.Device.Name, "Z-ABC1", "Does not use the stored data")
}
