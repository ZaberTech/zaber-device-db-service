package testgo

import (
	"os"

	"gotest.tools/v3/assert"

	"testing"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/api"
	cl "gitlab.com/ZaberTech/zaber-device-db-service/pkg/client"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/errors"
)

const userAgent = "Go Test 0.1"

var testURL = getEnv("TEST_HOST", "http://localhost:8080")

func TestClient_ReturnsInformation(t *testing.T) {
	client, err := cl.NewClient(testURL, userAgent)
	if err != nil {
		t.Fatal(err)
	}

	query := &api.GetProductInformationQuery{
		DeviceID: "30222",
		FW:       &dto.FirmwareVersion{Major: 6, Minor: 29, Build: 1432},
		Modified: true,
		Peripherals: []api.Peripheral{
			{PeripheralID: "43051"},
			{PeripheralID: "43052", Modified: true},
		},
	}

	info, err := client.GetProductInformation(query)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, info.Device.ProductID, 2743594)
	assert.Equal(t, info.Device.Name, "X-MCB2")

	p1 := info.Peripherals[0]
	assert.Equal(t, p1.Device.ProductID, 2743765)
	assert.Equal(t, p1.Device.Name, "LSM200A-T4 (rev 1)")

	p2 := info.Peripherals[1]
	assert.Equal(t, p2.Device.ProductID, 2743766)
	assert.Equal(t, p2.Device.Name, "LSM200B-T4 (rev 1)")
}

func TestClient_ThrowsErrorWhenNotFound(t *testing.T) {
	client, err := cl.NewClient(testURL, userAgent)
	if err != nil {
		t.Fatal(err)
	}

	query := &api.GetProductInformationQuery{
		DeviceID: "123",
		FW:       &dto.FirmwareVersion{Major: 6, Minor: 29, Build: 1432},
	}

	_, err = client.GetProductInformation(query)
	assert.Assert(t, err != nil)
	assert.Equal(t, err.ErrorType(), errors.ErrorNoRecordFound)
	assert.Equal(t, err.Error(), "Cannot find product with device ID 123 and firmware version 6.29.1432")
}

func TestClient_ReturnsInformationForBinaryDevice(t *testing.T) {
	client, err := cl.NewClient(testURL, userAgent)
	if err != nil {
		t.Fatal(err)
	}

	info, err := client.GetBinaryProductInformation(
		"30222",
		&dto.FirmwareVersion{Major: 6, Minor: 29, Build: 1432},
		"43051")
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, info.Device.ProductID, 2743594)
	assert.Equal(t, info.Device.Name, "X-MCB2")

	p := info.Peripheral
	assert.Equal(t, p.Device.ProductID, 2743765)
	assert.Equal(t, p.Device.Name, "LSM200A-T4 (rev 1)")
}

func getEnv(key string, defaultValue string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return defaultValue
}
