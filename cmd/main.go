package main

import (
	"gitlab.com/ZaberTech/zaber-device-db-service/internal/server"
)

func main() {
	serverInstace := server.NewServer()
	serverInstace.Start()
}
