# How to release

* Make sure that API version has been bumped if there are changes to API (`pkg/api/service.go`)
* Move release branch to tip of the master by ``git branch -f release $(git rev-parse master)``
* Push release branch
* Let it build on CI
* Trigger a manual job called "deploy_service_staging"
* Test that the staging build is fine by running a test script (or unit tests) with `Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, 'https://api-staging.zaber.io/device-db/public')`
* Trigger manual job called "deploy_service_production"
