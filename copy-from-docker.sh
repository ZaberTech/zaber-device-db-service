#!/bin/bash
set -e

id=$(docker create zaber/db-service:latest)
docker cp $id:/home/ubuntu/go/src/zaber-db-service/artifacts.tar.gz artifacts.tar.gz
docker rm -v $id
