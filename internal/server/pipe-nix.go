// +build !windows

package server

import (
	"net"
	"os"
)

func openPipe(endpoint string) (net.Listener, error) {
	os.Remove(endpoint)
	l, err := net.Listen("unix", endpoint)
	if err != nil {
		return nil, err
	}
	err = os.Chmod(endpoint, 0600)
	if err != nil {
		return nil, err
	}
	return l, nil
}
