package server

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/api"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/service"
	"gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

type server struct {
	service service.FileService
	echo    *echo.Echo
}

type Server interface {
	Start()
}

func NewServer() Server {
	dbFilePath := os.Getenv("ZABER_DEVICE_DB_PATH")
	if dbFilePath == "" {
		panic("environment variable ZABER_DEVICE_DB_PATH is not set")
	}
	serviceInstance := service.NewService(dbFilePath)

	echoInstance := echo.New()
	echoInstance.Logger.SetOutput(io.Discard)
	echoInstance.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodHead},
	}))
	echoInstance.Use(middleware.Logger())
	echoInstance.Use(middleware.Recover())

	instance := &server{
		service: serviceInstance,
		echo:    echoInstance,
	}

	urlPrefix := os.Getenv("URL_PREFIX")
	echoInstance.GET(urlPrefix+"/device/:deviceID", instance.getDevice)
	echoInstance.GET(urlPrefix+"/device/:deviceID/default-settings", instance.getDefaultSettings)
	echoInstance.GET(urlPrefix+"/device-binary/:deviceID", instance.getBinaryDevice)
	echoInstance.GET(urlPrefix+"/product/search", instance.getProductSearch)
	echoInstance.GET(urlPrefix+"/product/:productID/supported-peripherals", instance.getSupportedPeripherals)
	echoInstance.GET(urlPrefix+"/product/:productID/servo-tuning-info", instance.getServoTuningInfo)
	echoInstance.GET(urlPrefix+"/health", instance.getHealth)
	echoInstance.GET(urlPrefix+"/checksum", instance.getChecksum)

	return instance
}

func (instance *server) quitOnInterrupt() {
	caughtSignal := make(chan os.Signal, 1)
	signal.Notify(caughtSignal, os.Interrupt, syscall.SIGTERM)
	<-caughtSignal
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()
	if err := instance.echo.Shutdown(ctx); err != nil {
		log.Fatal(err)
	}
}

func (instance *server) Start() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	if port == "pipe" {
		pipeName, err := communication.GetDbServicePipe()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Opening database service locally at: %s\n", pipeName)
		listener, err := openPipe(pipeName)
		if err != nil {
			log.Fatal(err)
		}
		defer listener.Close()
		instance.echo.Listener = listener
		go instance.quitOnInterrupt()
		if err := instance.echo.StartServer(instance.echo.Server); err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
		}
	} else {
		if err := instance.echo.Start(":" + port); err != nil {
			log.Fatal(err)
		}
	}
}

func (instance *server) getDevice(context echo.Context) error {
	query := context.Request().URL.Query()

	productQuery := &api.GetProductInformationQuery{
		DeviceID: context.Param("deviceID"),
	}

	if fwString, has := query["fw"]; has {
		if parsedFw, err := dto.ParseFWVersion(fwString[0]); err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, "Cannot parse firmware version")
		} else {
			productQuery.FW = &parsedFw
		}
	}

	if modifiedStr, has := query["modified"]; has {
		if modified, err := strconv.ParseBool(modifiedStr[0]); err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, "Cannot parse modified")
		} else {
			productQuery.Modified = modified
		}
	}

	for _, peripheral := range query["peripheral"] {
		productQuery.Peripherals = append(productQuery.Peripherals, api.Peripheral{
			PeripheralID: peripheral,
		})
	}

	if peripheralsModified, has := query["peripheral-modified"]; has {
		if len(peripheralsModified) != len(productQuery.Peripherals) {
			return echo.NewHTTPError(http.StatusBadRequest, "peripheral-modified was not specified the same times as peripheral")
		}

		for i, modifiedStr := range peripheralsModified {
			if modified, err := strconv.ParseBool(modifiedStr); err != nil {
				return echo.NewHTTPError(http.StatusBadRequest, "Cannot parse peripheral-modified")
			} else {
				productQuery.Peripherals[i].Modified = modified
			}
		}
	}

	device, err := instance.service.GetProductInformation(productQuery)
	if err != nil {
		return instance.transformError(err)
	}
	if isFirmwareCacheable(productQuery.FW) {
		setImmutableHeader(context)
	}
	return context.JSON(http.StatusOK, device)
}

func (instance *server) getBinaryDevice(context echo.Context) error {
	query := context.Request().URL.Query()

	deviceID := context.Param("deviceID")

	var fw *dto.FirmwareVersion
	fwString, hasFw := query["fw"]
	if hasFw {
		if parsedFw, err := dto.ParseFWVersion(fwString[0]); err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, "Cannot parse firmware version")
		} else {
			fw = &parsedFw
		}
	}

	peripheralQuery := query["peripheral"]
	var peripheralID string
	if len(peripheralQuery) > 0 {
		peripheralID = peripheralQuery[0]
	}

	device, err := instance.service.GetBinaryProductInformation(deviceID, fw, peripheralID)
	if err != nil {
		return instance.transformError(err)
	}
	if isFirmwareCacheable(fw) {
		setImmutableHeader(context)
	}
	return context.JSON(http.StatusOK, device)
}

func (instance *server) getSupportedPeripherals(context echo.Context) error {
	productID, conversionErr := strconv.Atoi(context.Param("productID"))
	if conversionErr != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "Cannot parse product ID")
	}

	peripherals, err := instance.service.GetSupportedPeripherals(productID)
	if err != nil {
		return instance.transformError(err)
	}
	setImmutableHeader(context)
	return context.JSON(http.StatusOK, peripherals)
}

func (instance *server) getServoTuningInfo(context echo.Context) error {
	idStr := context.Param("productID")
	productID, conversionErr := strconv.Atoi(idStr)
	if conversionErr != nil {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Cannot parse product ID %s to int", idStr))
	}

	tuningVersion, err := instance.service.GetServoTuningInfo(productID)
	if err != nil {
		return instance.transformError(err)
	}
	setImmutableHeader(context)
	return context.JSON(http.StatusOK, tuningVersion)
}

func (instance *server) getDefaultSettings(context echo.Context) error {
	query := context.Request().URL.Query()

	deviceID := context.Param("deviceID")

	var fw *dto.FirmwareVersion
	fwString, hasFw := query["fw"]
	if hasFw {
		if parsedFw, err := dto.ParseFWVersion(fwString[0]); err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, "Cannot parse firmware version")
		} else {
			fw = &parsedFw
		}
	}

	peripheralIDs := query["peripheral"]

	values, err := instance.service.GetDefaultSettings(deviceID, fw, peripheralIDs)
	if err != nil {
		return instance.transformError(err)
	}

	if isFirmwareCacheable(fw) {
		setImmutableHeader(context)
	}
	return context.JSON(http.StatusOK, values)
}

func (instance *server) getProductSearch(context echo.Context) error {
	query := context.Request().URL.Query()

	dbQuery := &api.ProductSearchQuery{}

	if name, hasName := query["name"]; hasName {
		dbQuery.Name = name[0]
	}

	if deviceID, has := query["device-id"]; has {
		dbQuery.DeviceID = deviceID[0]
	}

	if dbQuery.Name == "" && dbQuery.DeviceID == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "No search parameters provided")
	}

	if fwString, hasFw := query["fw"]; hasFw {
		if parsedFw, err := dto.ParseFWVersion(fwString[0]); err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, "Cannot parse firmware version")
		} else {
			dbQuery.FW = &parsedFw
		}
	}

	if limit, hasLimit := query["limit"]; hasLimit {
		dbQuery.Limit, _ = strconv.Atoi(limit[0])
	}

	if dbQuery.Limit <= 0 {
		dbQuery.Limit = 10
	} else if dbQuery.Limit > 500 {
		return echo.NewHTTPError(http.StatusBadRequest, "Limit over 500 not supported")
	}

	products, err := instance.service.SearchProduct(dbQuery)
	if err != nil {
		return instance.transformError(err)
	}
	return context.JSON(http.StatusOK, products)
}

func (instance *server) getHealth(context echo.Context) error {
	err := instance.service.GetHealth()
	if err != nil {
		return instance.transformError(err)
	}
	return context.String(http.StatusOK, "OK")
}

func (instance *server) getChecksum(context echo.Context) error {
	checksum, err := instance.service.GetFileChecksum()
	if err != nil {
		return instance.transformError(err)
	}
	return context.String(http.StatusOK, checksum)
}
