package server

import (
	"net/http"

	"github.com/labstack/echo"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/client"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/errors"
)

func isFirmwareCacheable(version *dto.FirmwareVersion) bool {
	if version == nil {
		return false
	}
	return version.Major >= 0 && version.Minor >= 0 && version.Minor < 99 && version.Build >= 0
}

func setImmutableHeader(c echo.Context) {
	query := c.Request().URL.Query()

	if hasAPIVersion := len(query.Get(client.APIVersionQueryParam)) > 0; hasAPIVersion {
		c.Response().Header().Set("Cache-Control", "public, max-age=604800, immutable")
	}
}

func (instance *server) transformError(err errors.Error) error {
	if err.ErrorType() == errors.ErrorNoRecordFound {
		return echo.NewHTTPError(http.StatusNotFound, err.Error())
	} else if err.ErrorType() == errors.ErrorChecksumNotAvailableYet {
		return echo.NewHTTPError(http.StatusAccepted, err.Error())
	}
	return err
}
