// +build windows

package server

import (
	"net"
	"gopkg.in/natefinch/npipe.v2"
)

func openPipe(endpoint string) (net.Listener, error) {
	return npipe.Listen(endpoint)
}
