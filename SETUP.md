To run locally, you must have a local copy of the device db. You can find it on Z Drive at:
```
\\fs\Zaber\Product\FW\device-database
```

To run, first install dependencies (`npm i`) and then run with:
```
npm start
```

## Local Development

### Get the service running locally

Download a copy of the database from Z drive. You can find the latest at
`\\fs\Zaber\Product\FW\device-database\current`

It is recommended to download the compressed db (.lzma) then you can extract the full database with:
`unlzma devices-public.sqlite.lzma`

With the db on your computer, you need to set an env variable to point to it's location, and specify the correct path extension. On nix systems this looks like:
```
export ZABER_DEVICE_DB_PATH=/users/you/path/to/devices-public.sqlite
export URL_PREFIX=device-db/master
```

Finally, install dependencies (`npm i`), and get the service running with:
`npm start`

### Using a local build in ZML

In ZMLs `go.mod`, add a [replace command](https://thewebivore.com/using-replace-in-go-mod-to-point-to-your-local-module/) like:
`replace gitlab.com/ZaberTech/zaber-device-db-service => /Users/pselle/Projects/zaber-device-db-service`

Then can run scripts with something like
`Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, 'http://localhost:8080');`
Add this in a test script, or the `beforeEach` function of e2e tests

### Using a local build in Launcher

To develop on Zaber Launcher:

* Get the service running locally
* Edit the `.env.staging` file of Launcher to set `API_URL=http://127.0.0.1:8080`
* Finally, comment out the `Content-Security-Policy` lines in `index.html` to allow the calls to the localhost (you can also add `127.0.0.1:8080`, but either way, it must be rolled back before commit)
