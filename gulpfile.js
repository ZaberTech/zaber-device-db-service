/* eslint-disable camelcase */
const child_process = require('child_process');
const { promisify } = require('bluebird');
const os = require('os');
const fs = require('fs');
const YAML = require('yaml');
const path = require('path');
const glob = promisify(require('glob'));
const { series } = require('gulp');
const packageJson = require('./package.json');

const gen_license = () => require('./licensing/generate').collectLicenses();

const exec = (...args) => new Promise((resolve, reject) => {
  const childProcess = child_process.exec(...args, err => {
    if (err) {
      return reject(err);
    }

    resolve();
  });
  childProcess.stdout.pipe(process.stdout);
  childProcess.stderr.pipe(process.stderr);
});


const extensions = {
  win32: '.exe',
};

const build_template = async (GOOS, GOARCH) => {
  const extension = extensions[os.platform()] || '';

  await exec(`go build -o bin/${GOOS}/${GOARCH}/zaber-device-db-service${extension} cmd/main.go`, {
    env: {
      GOOS,
      GOARCH,
      ...process.env,
    }
  });
};

const build_linux_386 = () => build_template('linux', '386');
const build_linux_amd64 = () => build_template('linux', 'amd64');
const build_linux_arm = () => build_template('linux', 'arm');
const build_linux = series(build_linux_386, build_linux_amd64);

const build_windows_386 = () => build_template('windows', '386');
const build_windows_amd64 = () => build_template('windows', 'amd64');
const build_windows = series(build_windows_386, build_windows_amd64);

const build_darwin_amd64 = () => build_template('darwin', 'amd64');
const build_darwin_arm64 = () => build_template('darwin', 'arm64');
const build_darwin = series(build_darwin_amd64, build_darwin_arm64);

const build = (() => {
  const platform = os.platform();
  const arch = os.arch();
  switch (platform) {
    case 'win32':
      return build_windows;
    case 'linux':
      if (arch === 'arm') {
        return build_linux_arm;
      }
      return build_linux;
    case 'darwin':
      return build_darwin;
    default:
      throw new Error('Unsupported OS');
  }
})();


const zip_binaries = async () => {
  const binaries = await glob(path.join('bin', '**', 'zaber-device-db-service*'));
  for (const binary of binaries) {
    const dir = path.dirname(binary);
    const dist = dir.replace('bin', 'dist');
    await exec([
      `mkdir -p ${dist}`,
      `cd ${dir}`,
      'cp ../../../licensing/LICENSE.txt .',
      `zip ../../../${dist}/zaber-device-db-service.zip ${path.basename(binary)} LICENSE.txt`,
    ].join(' && '));
  }
};

const publish = async () => {
  await zip_binaries();

  const toFolder = `downloads/device-db-service/${packageJson.version}/`;
  await exec(`aws s3 cp dist/ ${process.env.BUCKET}/${toFolder} --recursive`);
};

const pack_artifacts = async () => {
  const content = YAML.parse(fs.readFileSync('.gitlab-ci.yml', 'utf-8'), { version: '1.1' });
  const artifactPaths = content.build_exe_linux.artifacts.paths;
  const files = [];
  for (const path of artifactPaths) {
    const matched = await glob(path);
    const filesOnly = matched.filter(file => fs.statSync(file).isFile());
    files.push(...filesOnly);
  }
  fs.writeFileSync('artifacts.txt', files.join(os.EOL), 'utf-8');
  await exec('tar -czf artifacts.tar.gz -T artifacts.txt');
};

module.exports = {
  gen_license,
  build,
  zip_binaries,
  publish,
  pack_artifacts,
};
