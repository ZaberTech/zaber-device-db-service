FROM golang:1.19-alpine3.16 as goimage
ENV GO111MODULE=on
RUN apk add --no-cache git build-base \
&& mkdir -p /go/src/
ADD . /go/src/zaber-device-db-service/
WORKDIR /go/src/zaber-device-db-service
RUN go mod download \
&& CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -o bin/main cmd/main.go
