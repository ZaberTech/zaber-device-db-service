const got = require('got');
const { expect } = require('chai');
const { TEST_HOST } = require('./const');
const { compareSnapshot } = require('./utils');
const { validateWithSchema } = require('./schema');

const TESTED_PRODUCT = 8630682;

describe('/product/:productID', () => {
  describe('/supported-peripherals', () => {
    it('returns information according to schema', async () => {
      const { body } = await got(`${TEST_HOST}/product/${TESTED_PRODUCT}/supported-peripherals`, { json: true });

      await validateWithSchema(body, 'product_supported_peripherals.yml');
      await compareSnapshot(body, 'product_supported_peripherals');
    });

    it('throws error when product does not support peripherals', async () => {
      const response = await got(`${TEST_HOST}/product/8636944/supported-peripherals`,
        { json: true, throwHttpErrors: false });

      expect(response.statusCode).to.eq(404);
      expect(response.body.message).to.eq('Product with product ID 8636944 does not exist or support peripherals');
    });
  });


  describe('/servo-tuning-info', () => {
    it('returns information according to schema', async () => {
      const { body } = await got(`${TEST_HOST}/product/15772829/servo-tuning-info`, { json: true });

      await validateWithSchema(body, 'servo_tuning_info.yml');
      await compareSnapshot(body, 'servo_tuning_info');
    });

    it('returns 404 error when product is not in public db', async () => {
      const response = await got(
        `${TEST_HOST}/product/1000000000/servo-tuning-info`,
        { json: true, throwHttpErrors: false }
      );

      expect(response.statusCode).to.eq(404);
      expect(response.body.message).to.eq(
        'Product with product ID 1000000000 does not exist or does not support servo tuning'
      );
    });
  });
});

describe('/product/search', () => {
  it('returns information according to schema', async () => {
    const { body } = await got(`${TEST_HOST}/product/search`, { json: true, query: { name: 'X-LSM___A' } });

    await validateWithSchema(body, 'product_search.yml');
    await compareSnapshot(body, 'product_search');
  });

  it('returns information according to schema (peripheral)', async () => {
    const { body } = await got(`${TEST_HOST}/product/search`, { json: true, query: { name: 'LSM___A' } });

    await validateWithSchema(body, 'product_search.yml');
    await compareSnapshot(body, 'product_search_peripheral');
  });

  it('respects limit', async () => {
    const { body } = await got(`${TEST_HOST}/product/search`, { json: true, query: { name: 'X-LSM%', limit: '13' } });
    expect(body.products.length).to.eq(13);
  });

  it('allows filter on a specific firmware version', async () => {
    const { body: result1 } = await got(`${TEST_HOST}/product/search`,
      { json: true, query: { name: 'T-NA%', fw: '5.9' } });
    expect(result1.products.length).to.eq(0);

    const { body: result2 } = await got(`${TEST_HOST}/product/search`,
      { json: true, query: { name: 'T-NA%', fw: '5.10' } });
    expect(result2.products.length).to.gt(0);

    const { body: result3 } = await got(`${TEST_HOST}/product/search`,
      { json: true, query: { name: 'T-NA%', fw: '5' } });
    expect(result3.products.length).to.gt(0);

    const { body: result4 } = await got(`${TEST_HOST}/product/search`,
      { json: true, query: { name: 'T-NA%', fw: '6' } });
    expect(result4.products.length).to.eq(0);
  });

  describe('device.id', () => {
    it('returns information according to schema', async () => {
      const { body } = await got(`${TEST_HOST}/product/search`, { json: true, query: { 'device-id': 50281 } });

      await validateWithSchema(body, 'product_search.yml');
      await compareSnapshot(body, 'product_search_id');
    });

    it('returns information according to schema (peripheral)', async () => {
      const { body } = await got(`${TEST_HOST}/product/search`, { json: true, query: { 'device-id': 46355 } });

      await validateWithSchema(body, 'product_search.yml');
      await compareSnapshot(body, 'product_search_id_peripheral');
    });

    it('allows filter on a specific firmware version (device.id)', async () => {
      const { body: result1 } = await got(`${TEST_HOST}/product/search`,
        { json: true, query: { 'device-id': 5081, 'fw': '5.9' } });
      expect(result1.products.length).to.eq(0);

      const { body: result2 } = await got(`${TEST_HOST}/product/search`,
        { json: true, query: { 'device-id': 5081, 'fw': '5.10' } });
      expect(result2.products.length).to.eq(1);

      const { body: result3 } = await got(`${TEST_HOST}/product/search`,
        { json: true, query: { 'device-id': 5081, 'fw': '5' } });
      expect(result3.products.length).to.eq(1);

      const { body: result4 } = await got(`${TEST_HOST}/product/search`,
        { json: true, query: { 'device-id': 5081, 'fw': '6' } });
      expect(result4.products.length).to.eq(0);
    });
  });
});
