const got = require('got');
const { expect } = require('chai');
const { TEST_HOST } = require('./const');
const { compareSnapshot } = require('./utils');
const { validateWithSchema } = require('./schema');

describe('/device', () => {
  it('returns information for integrated device', async () => {
    const { body } = await got(`${TEST_HOST}/device/50041?fw=6.29.1432`, { json: true });

    await validateWithSchema(body, 'device.yml');
    await compareSnapshot(body, 'integrated_device');
  });

  it('returns information for controller with peripherals', async () => {
    const { body } = await got(
      `${TEST_HOST}/device/30222?fw=6.29.1432&peripheral=43052&peripheral=43051`,
      { json: true }
    );

    await validateWithSchema(body, 'device.yml');
    await compareSnapshot(body, 'controller_device');

    expect(
      body.peripherals.map(peripheral => peripheral.device.device_id)
    ).deep.eq([43052, 43051], 'Peripherals not in requested order');
  });

  it('returns information for integrated device with multiple peripheral-like axes', async () => {
    const { body } = await got(
      `${TEST_HOST}/device/50952?fw=7.15.8617&peripheral=70315&peripheral=70316`,
      { json: true }
    );

    await validateWithSchema(body, 'device.yml');
    await compareSnapshot(body, 'integrated_mutli_axes_device');

    expect(
      body.peripherals.map(peripheral => peripheral.device.device_id)
    ).deep.eq([70315, 70316], 'Peripherals not in requested order');
  });

  it('returns valid information for controller device with 3rd party peripheral', async () => {
    const { body } = await got(
      `${TEST_HOST}/device/30312?fw=7.27.12025&peripheral=88800`,
      { json: true }
    );

    await validateWithSchema(body, 'device.yml');
  });

  it('returns information for device returning last firmware if not specified', async () => {
    const { body } = await got(`${TEST_HOST}/device/302`, { json: true });

    expect(body.device).to.deep.include({
      fw: { major: 5, minor: 35, build: 0 },
    });
  });

  it('returns information for device with specific firmware', async () => {
    const { body } = await got(`${TEST_HOST}/device/50041?fw=6.28`, { json: true });

    expect(body.device).to.deep.include({
      fw: { major: 6, minor: 28, build: 1347 },
    });
  });

  it('returns information for device with specific firmware and unknown build number', async () => {
    const { body } = await got(`${TEST_HOST}/device/50041?fw=6.28.6666`, { json: true });

    expect(body.device).to.deep.include({
      fw: { major: 6, minor: 28, build: 1347 },
    });
  });

  it('returns 404 when device does not exist', async () => {
    const response = await got(`${TEST_HOST}/device/500`, { json: true, throwHttpErrors: false });

    expect(response.statusCode).to.eq(404);
    expect(response.body.message).to.eq('Cannot find product with device ID 500 and firmware version <nil>');
  });

  it('returns 404 when device with firmware version does not exist', async () => {
    const response = await got(`${TEST_HOST}/device/50041?fw=5.28`, { json: true, throwHttpErrors: false });

    expect(response.statusCode).to.eq(404);
    expect(response.body.message).to.eq('Cannot find product with device ID 50041 and firmware version 5.28');
  });

  it('returns 404 when peripheral does not exist', async () => {
    const response = await got(
      `${TEST_HOST}/device/30222?fw=6.29.1432&peripheral=43050`,
      { json: true, throwHttpErrors: false }
    );

    expect(response.statusCode).to.eq(404);
    expect(response.body.message).to.eq('Cannot find product with peripheral ID 43050 and parent product ID 2743594');
  });

  it('returns binary information for integrated device', async () => {
    const { body } = await got(`${TEST_HOST}/device-binary/50041?fw=6.29.1432`, { json: true });

    await validateWithSchema(body, 'device_binary.yml');
    await compareSnapshot(body, 'integrated_device_binary');
  });

  it('returns binary information for controller with peripherals', async () => {
    const { body } = await got(
      `${TEST_HOST}/device-binary/30222?fw=6.29.1432&peripheral=43051`,
      { json: true }
    );

    await validateWithSchema(body, 'device_binary.yml');
    await compareSnapshot(body, 'controller_device_binary');
  });

  it('returns binary information for integrated device with multiple peripheral-like axes', async () => {
    const { body } = await got(
      `${TEST_HOST}/device-binary/50952?fw=7.15.8617&peripheral=70315`,
      { json: true }
    );

    await validateWithSchema(body, 'device_binary.yml');
    await compareSnapshot(body, 'integrated_multi_axes_device_binary');
  });

  it('returns information for modified device with peripherals', async () => {
    const { body } = await got(
      `${TEST_HOST}/device/30341?${[
        'fw=7.21.9827', // TODO: 7.22
        'modified=1',
        'peripheral=63221',
        'peripheral-modified=1',
        'peripheral=63221',
        'peripheral-modified=0'
      ].join('&')}`,
      { json: true }
    );

    await validateWithSchema(body, 'device.yml');
    await compareSnapshot(body, 'controller_device_modified');

    expect(body.peripherals[0].settings.rows.find(setting => setting.name === 'cloop.enable')).to.exist;
  });

  it('returns information for modified integrated device', async () => {
    const { body } = await got(
      `${TEST_HOST}/device/50081?${[
        'fw=7.21.9827', // TODO: 7.22
        'modified=1',
      ].join('&')}`,
      { json: true }
    );

    await validateWithSchema(body, 'device.yml');
    await compareSnapshot(body, 'integrated_device_modified');

    expect(body.settings.rows.find(setting => setting.name === 'cloop.enable')).to.exist;
  });

  it('throws error when peripheral-modified count does not match peripheral count', async () => {
    const response = await got(
      `${TEST_HOST}/device/30341?${[
        'peripheral=43211',
        'peripheral=43211',
        'peripheral-modified=0'
      ].join('&')}`,
      { json: true, throwHttpErrors: false }
    );

    expect(response.statusCode).to.eq(400);
    expect(response.body.message).to.eq('peripheral-modified was not specified the same times as peripheral');
  });
});
