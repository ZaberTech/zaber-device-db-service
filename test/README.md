# Integration tests

- Setup the service to run at <http://localhost:8080/device-db/public>
    - Make sure to use the `public` database using the `ZABER_DEVICE_DB_PATH` env variable
    - Make sure to set the URL for your local instance of the service properly using the `URL_PREFIX` env variable
    - Example on windows

        ```
        set ZABER_DEVICE_DB_PATH=<path_to_databases>/devices-public.sqlite
        set URL_PREFIX=device-db/public
        npm start
        ```
    - on unix like OSes:
        ```
        export ZABER_DEVICE_DB_PATH=<path_to_databases>/devices-public.sqlite
        export URL_PREFIX=device-db/public
        npm start
        ```
- Run the tests like this (set -> export for unix)

    ```
    set TEST_HOST=http://localhost:8080/device-db/public
    npm run test:integration
    ```

## Handling schema changes

- If you had made changes to the structure of the data returned by the API then you might need to change the appropriate schema in the `./schemas` directory so that it reflects the new structure
- You will also need to update the snapshots that are used for comparing the current JSON output from the API to an expected snapshot JSON output
    - You can update the snapshots like this (example on Windows)

        ```
        set UPDATE_SNAPSHOTS=1
        npm run test:integration
        set UPDATE_SNAPSHOTS=
        ```
    - This will update the reference JSON data stored in the `./snapshots` directory
    - Make sure that the snapshots are created using the `public` database
