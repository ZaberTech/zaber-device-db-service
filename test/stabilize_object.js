const crypto = require('crypto');

// TODO (Martin 2020-04): consider https://github.com/epoberezkin/fast-json-stable-stringify

function findSuperKey(arrayOfObjects) {
  if (typeof arrayOfObjects[0] !== 'object') {
    return null;
  }
  const sampleKeys = Object.keys(arrayOfObjects[0]).sort();
  while (sampleKeys.length) {
    const testedKey = sampleKeys.shift();
    const valueTest = new Set();

    for (const obj of arrayOfObjects) {
      const value = obj[testedKey];
      if (typeof value !== 'string' || valueTest.has(value)) {
        break;
      }
      valueTest.add(value);
    }

    if (valueTest.size === arrayOfObjects.length) {
      return testedKey;
    }
  }

  return null;
}

function stabilizeObject(object) {
  if (object === null) {
    return 'null';
  } if (object === undefined) {
    return 'undefined';
  } else if (typeof object === 'number' || typeof object === 'boolean') {
    return String(object);
  } else if (typeof object === 'string') {
    return JSON.stringify(object);
  } else if (Array.isArray(object)) {
    const superKey = findSuperKey(object);

    const withHashes = object.map(item => {
      const hash = stabilizeObject(item);
      const sortKey = (superKey && item[superKey]) || hash;
      return { item, hash, sortKey };
    });

    withHashes.sort((a, b) => a.sortKey.localeCompare(b.sortKey));

    const md5 = crypto.createHash('md5');

    withHashes.forEach(({ item, hash }, i) => {
      object[i] = item;
      md5.update(`${hash},`, 'utf8');
    });

    return md5.digest('hex');
  } else if (typeof object === 'object') {
    const md5 = crypto.createHash('md5');

    for (const key of Object.keys(object).sort()) {
      const hash = stabilizeObject(object[key]);
      md5.update(`${key}:${hash},`, 'utf8');
    }

    return md5.digest('hex');
  } else {
    throw new Error(`Cannot handle object: ${JSON.stringify(object)}`);
  }
}

module.exports = {
  stabilizeObject,
};
