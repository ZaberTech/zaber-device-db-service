const got = require('got/source');
const os = require('os');
const { TEST_HOST } = require('./const');

exports.mochaGlobalSetup = async function() {
  process.stdout.write('Waiting for server to start...');
  const start = Date.now();
  for (;;) {
    try {
      await got(`${TEST_HOST}/device/500`, { json: true, throwHttpErrors: false });
      process.stdout.write(`ready${os.EOL}`);
      return;
    } catch (err) {
      if (Date.now() - start > 180 * 1000) {
        throw err;
      }
      process.stdout.write('.');
    }
  }
};
