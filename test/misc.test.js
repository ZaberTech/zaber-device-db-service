const got = require('got');
const { expect } = require('chai');
const { TEST_HOST } = require('./const');

describe('/health', () => {
  it('returns information according to schema', async () => {
    const { body } = await got(`${TEST_HOST}/health`);
    expect(body).to.eq('OK');
  });
});
describe('/checksum', () => {
  it('returns checksum of the source file', async function() {
    this.timeout(120 * 1000);

    let result;
    do {
      if (result != null) {
        await new Promise(r => setTimeout(r, 1000));
      }
      result = await got(`${TEST_HOST}/checksum`);
    } while (result.statusCode === 202);

    expect(result.statusCode).to.eq(200);
    expect(result.body).to.match(/^[0-9a-f]{32}$/);
  });
});
