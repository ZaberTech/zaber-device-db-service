const got = require('got');
const { expect } = require('chai');
const { TEST_HOST } = require('./const');
const { compareSnapshot } = require('./utils');
const { validateWithSchema } = require('./schema');

describe('/device/default-settings', () => {
  it('returns information for integrated device', async () => {
    const { body } =
      await got(`${TEST_HOST}/device/50105/default-settings?fw=7.24.10913`,
        { json: true });

    await validateWithSchema(body, 'default_settings.yml');
    await compareSnapshot(body, 'default_settings_xlsq150a');
  });


  it('returns information for controller with peripherals', async () => {
    const { body } =
      await got(`${TEST_HOST}/device/30222/default-settings?peripheral=44112&peripheral=44122&fw=7.24.10913`,
        { json: true });

    await validateWithSchema(body, 'default_settings.yml');

    // Note compareSnapshot reorders array elements by hash, so the correct ordering of returned
    // results must be checked separately first.
    expect(body.peripherals[0].peripheral_id).to.eq(44112);
    expect(body.peripherals[1].peripheral_id).to.eq(44122);
    await compareSnapshot(body, 'default_settings_xmcb2_asr');
  });


  it('returns information for latest firmware version if not specified', async () => {
    const { body } = await got(`${TEST_HOST}/device/302/default-settings`, { json: true });

    expect(body).to.deep.include({
      fw: { major: 5, minor: 35, build: 0 },
    });
  });

  it('returns 404 when device does not exist', async () => {
    const response = await got(`${TEST_HOST}/device/500/default-settings`, { json: true, throwHttpErrors: false });

    expect(response.statusCode).to.eq(404);
    expect(response.body.message).to.eq('Cannot find product with device ID 500 and firmware version <nil>');
  });

  it('returns 404 when device is not specified', async () => {
    const response = await got(`${TEST_HOST}/device/default-settings`, { json: true, throwHttpErrors: false });

    expect(response.statusCode).to.eq(404);
    expect(response.body.message)
      .to.eq('Cannot find product with device ID default-settings and firmware version <nil>');
  });

  it('returns 404 when device with firmware version does not exist', async () => {
    const response = await got(`${TEST_HOST}/device/50105/default-settings?fw=4.0.4`,
      { json: true, throwHttpErrors: false });

    expect(response.statusCode).to.eq(404);
    expect(response.body.message).to.eq('Cannot find product with device ID 50105 and firmware version 4.0.4');
  });

  it('returns 404 when peripheral does not exist', async () => {
    const response = await got(
      `${TEST_HOST}/device/30222/default-settings?peripheral=1234`,
      { json: true, throwHttpErrors: false }
    );

    expect(response.statusCode).to.eq(404);
    expect(response.body.message).to.match(/Cannot find product with peripheral ID 1234 and parent product ID \d+/);
  });
});
