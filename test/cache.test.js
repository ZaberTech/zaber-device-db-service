const got = require('got');
const { expect } = require('chai');
const { TEST_HOST } = require('./const');

describe('caching headers', () => {
  it([
    'provides immutable caching header',
    'only when URL contains "api-version" and "fw" that is fully specified version'
  ].join(' '),
  async () => {
    const response1 = await got(`${TEST_HOST}/device/30222?api-version=1.4&fw=7.21.9827`, { json: true });
    const response2 = await got(`${TEST_HOST}/device/30222?api-version=1.4&fw=7.21`, { json: true });
    const response3 = await got(`${TEST_HOST}/device/30222?fw=7.21.9827`, { json: true });
    const response4 = await got(`${TEST_HOST}/device/30222?api-version=1.4`, { json: true });
    const response5 = await got(`${TEST_HOST}/device/30222`, { json: true });

    expect(response1.headers['cache-control']).to.eq('public, max-age=604800, immutable');
    expect(response2.headers['cache-control']).to.be.undefined;
    expect(response3.headers['cache-control']).to.be.undefined;
    expect(response4.headers['cache-control']).to.be.undefined;
    expect(response5.headers['cache-control']).to.be.undefined;
  });
  it('does not provide immutable caching header for error', async () => {
    const response = await got(`${TEST_HOST}/device/1?api-version=1.4`, { json: true, throwHttpErrors: false });

    expect(response.statusCode).to.eq(404);
    expect(response.headers['cache-control']).to.be.undefined;
  });
  it('does not provide immutable caching header for health request', async () => {
    const response = await got(`${TEST_HOST}/health?api-version=1.4`);
    expect(response.headers['cache-control']).to.be.undefined;
  });
});
