const Ajv = require('ajv');
const path = require('path');
const { loadYaml } = require('./utils');

const ajv = new Ajv();

const validateWithSchema = async (data, schemaFilename) => {
  const file = path.join(__dirname, 'schemas', schemaFilename);
  const schema = await loadYaml(file);

  const validate = ajv.compile(schema);
  const isValid = validate(data);
  if (!isValid) {
    throw new Error(`Schema validation failed: ${JSON.stringify(validate.errors, null, 2)}`);
  }
};

module.exports = {
  validateWithSchema,
};
