const fs = require('fs');
const _ = require('lodash');
const yaml = require('js-yaml');
const { expect } = require('chai');
const { stabilizeObject } = require('./stabilize_object');

async function loadYaml(file) {
  const content = await new Promise((resolve, reject) =>
    fs.readFile(file, 'utf8', (err, content) => {
      if (err) {
        reject(err);
      } else {
        resolve(content);
      }
    })
  );

  return yaml.safeLoad(content);
}

async function loadJSON(file) {
  const content = await new Promise((resolve, reject) =>
    fs.readFile(file, 'utf8', (err, content) => {
      if (err) {
        reject(err);
      } else {
        resolve(content);
      }
    })
  );

  return JSON.parse(content);
}

function saveJSON(file, content) {
  return new Promise((resolve, reject) =>
    fs.writeFile(file, JSON.stringify(content, null, 2), 'utf8', err => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    })
  );
}

async function compareSnapshot(data, testName) {
  data = _.cloneDeep(data);
  stabilizeObject(data);

  const file = `${__dirname}/snapshots/${testName}.json`;

  if (process.env.UPDATE_SNAPSHOTS) {
    await saveJSON(file, data);
  }

  const snapshot = await loadJSON(file);
  expect(data).to.deep.equal(snapshot);
}

module.exports = {
  loadJSON,
  saveJSON,
  loadYaml,
  compareSnapshot,
};
