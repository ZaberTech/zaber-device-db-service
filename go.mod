module gitlab.com/ZaberTech/zaber-device-db-service

go 1.18

require (
	github.com/labstack/echo v3.3.10+incompatible
	github.com/mattn/go-sqlite3 v1.11.0
	gitlab.com/ZaberTech/zaber-go-lib v0.0.0-20221209231409-693c64b031b0
	gopkg.in/natefinch/npipe.v2 v2.0.0-20160621034901-c1b8fa8bdcce
	gotest.tools/v3 v3.0.2
)

require (
	github.com/creack/goselect v0.1.2 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/labstack/gommon v0.2.8 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v0.0.0-20170224212429-dcecefd839c4 // indirect
	github.com/zabertech/go-serial v0.0.0-20210201195853-2428148c5139 // indirect
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550 // indirect
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	golang.org/x/text v0.3.0 // indirect
)
